#! /usr/bin/python
import os

def create_workload():
    #parsecs = ['blackscholes', 'freqmine', 'x264', 'ferret']
    parsecs = ['x264']
    for parsec in parsecs:
        create_workload_with(parsec)

def create_workload_with(parsec):
    #for input_size in ['simsmall', 'simsmall', 'simsmall', 'simmedium', 'simmedium', 'simlarge',  'simmedium', 'simmedium', 'simsmall',  'simsmall', 'simsmall', 'simmedium', 'simmedium', 'simlarge', 'simmedium', 'simmedium', 'simsmall', 'simsmall', 'simsmall']:
    input_size = 'native'
    cmd = "parsecmgmt -c gcc-hooks-poet -a run -p " + parsec + " -i " + input_size + " -n 4"
    os.system(cmd)

if __name__ == '__main__':
    import sys
    benchmark = sys.argv[1]
    create_workload_with(benchmark)
