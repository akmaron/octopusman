#! /usr/bin/python
import os 
from operator import itemgetter
import sys

if len(sys.argv) != 3:
    print('usage: ./move-parmibench-jobs.py application [core-mapping]')
    exit()
app = sys.argv[1]
cores = sys.argv[2]

if app == 'sha':
    cmd = 'ps aux | grep "Security/sha"'
else:
    cmd = 'ps aux | grep "Network/Dijkstra/Parallel"'

f = os.popen(cmd)
lines = [line.split() for line in f.readlines()]

lines.sort(key=lambda x: x[2], reverse=True)
pid = lines[0][1]
#only 1 process for a signle parsec application(muliple threads though)
#get cores from the command line
#move parent process to other cores
cmd = 'taskset -c -p ' + cores[1:-1] + ' ' + pid 
os.system(cmd)
#move threads
cmd = 'ls /proc/' + pid + '/task/'
f = os.popen(cmd)
lines = [line.split() for line in f.readlines()]
for l in lines:
    tid = l[0]
    os.system('taskset -c -p ' + cores[1:-1] + ' ' +tid)
