import numpy as np
import pandas as pd
import ast
import argparse
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as pyplot
import os

CORE_MAPPING = {"[0]":0,"[0, 1]":1,"[0, 1, 2]":2,"[0, 1, 2, 3]":3, 
                "[4]":4, "[4, 5]":5, "[4, 5, 6]":6, "[4, 5, 6, 7]":7}


def parse_log_file(filename):
    """
    This method loads the Heartbeats' log file, remove unnecessary columns and put all data
    into a Pandas DataFrame.

    Timestamp is in nanoseconds
    Instant_Delay is in miliseconds
    Instant_Power is in Watts

    :param filename: Heartbeats log file to be parsed
    :return data_log: Pandas Dataframe qith the following columns: Timestamp, Instant_Delay, Instant_Power
    """
    columns = ['Timestamp','Instant_Latency','Instant_Power']
    data_log = pd.read_table(filename,usecols=columns)
    data_log['Instant_Latency'] = data_log['Instant_Latency']/(10**6)
    data_log = data_log[1:]

    return data_log

def parse_mapping_data(filename):
    """ This data is easily parsed when applied to the 
          hetero mapper file that was not cleaned by the 
          './cleanupFile.sh' script 

    :param filename: OctopusMan's log file to be parsed
    :return : list of strings. The string indicates the mapping being used ([0],[0, 1] ... [4, 5, 6, 7])
    """
       
    data = open(filename).readlines()
    # ast.literal_eval transforms the string "[0, 1]" into a python list [0, 1], for example.
    return [ast.literal_eval(l.split(";")[4].strip()) for l in data]


def count_violations(df, deadline, mapping_interval):
    """
    Counts the number of violations for each mapping interval

    :param df: Pandas DataFrame containing the logged data
    :param deadline: Deadline used in the experiment
    :return violations: a list of the number of violations in each mapping interval
    """

    iteraction = 0
    first_ts = df['Timestamp'][1]
    end_interval = mapping_interval * (10 ** 9)
    violations = [] # list with the number of violations in the interval
    violations.append(0)
    for ts,delay in zip(df['Timestamp'], df['Instant_Latency']):
        if(ts - first_ts) >= end_interval:
            iteraction += 1
            violations.append(0)
            first_ts = ts
        if delay > deadline:
            violations[-1] += 1

    return violations


def average_latency(df,interval):
    """
    Computes the 95th percentile latency (in ms) for each mapping interval.

    :param df: Pandas DataFrame containing the logged data
    :param interval: Mapping interval used in the experiment
    :return avg_latency: List of the 95th percentile latency of each mapping interval
    """

    first_ts = df['Timestamp'][1]
    end_interval = interval * (10 ** 9)
    avg_latency = [] # list with the number of violations in the interval
    interval_latencies = []
    for ts,delay in zip(df['Timestamp'], df['Instant_Latency']):
        if(ts - first_ts) >= end_interval:
            avg_latency.append(np.percentile(interval_latencies,95))
            interval_latencies = []
            first_ts = ts
        interval_latencies.append(delay)

    return avg_latency


def power_consumption(df, interval):
    """
    Retrieves the total power consumption in each mapping interval

    :param df: Pandas DataFrame containing the logged data
    :param interval: Mapping interval used in the experiment
    :return power_data: A list with the total power consumption in each mapping interval
    """

    iteraction = 0
    first_ts = df['Timestamp'][1]
    end_interval = interval * (10 ** 9)
    power_data = [] # list with the total power consumption in each mapping interval
    power_data.append(0)
    for ts,power in zip(df['Timestamp'], df['Instant_Power']):
        if(ts - first_ts) >= end_interval:
            power_data.append(0)
            first_ts = ts
        power_data[-1] += power

    return power_data


def make_plots(violations,avg_latency,power,mappingData,folder,parameters,deadline):
    """
    Creates 4 plots in the same figure:
      - Workload
      - Latency
      - Power
      - Core Mapping

    :param workload: a list of 1/#of_heartbeat for each mapping interval
    :param violations: a list of the number of violations in each mapping interval
    :param avg_latency: List of the 95th percentile latency of each mapping interval
    :param power: A list with the total power consumption in each mapping interval
    :param mappingData: list of strings containing the core mapping in each mappping interval
    :param folder: path to folder where plots and summary data should be stored
    :param parameters: parameters used in the experiment
    :param deadline: deadline used in the experiment
    :return: Nothing
    """

    titleFontSize = 10
    labelFontSize = 8
    tickFontSize = 8
    linewidth = 1
    interval = int(parameters.split('-')[1])

    # Plot upper graph with the number of violations in each timestamp  
    #pyplot.subplot(4, 1, 1)
    #xData = [i for i in range(len(workload))]
    #pyplot.plot(xData, workload, '-', label='Workload', linewidth=linewidth)
    # pyplot.ylim([0,40])
    # pyplot.xlabel('Timestamp')
    #pyplot.ylabel('1/#heartbeats',fontsize=labelFontSize)
    #pyplot.title(parameters+'\nWorkload',fontsize=titleFontSize)
    #set_ticks_size(pyplot.gca(),tickFontSize)

    # # Plot upper graph with the number of violations in each timestamp  
    # pyplot.subplot(4, 1, 2)
    # xData = [i for i in range(len(violations))]
    # pyplot.plot(xData, violations.values(), 'r-', label='Violations', linewidth=linewidth)
    # pyplot.ylim([0,40])
    # # pyplot.xlabel('Timestamp')
    # pyplot.ylabel('Number of Violations',fontsize=labelFontSize)
    # pyplot.title('Violations',fontsize=titleFontSize)
    # SetTicksSize(pyplot.gca(),tickFontSize)

    # Plot mid graph with the 95th percentile latency in each mapping interval
    pyplot.subplot(3, 1, 1)
    xData = range(len(avg_latency))
    deadline_line = np.ones(len(xData))*deadline
    pyplot.plot(xData, avg_latency, color = 'g', marker = 'o', markersize = 2.0, linestyle = '', label='95th Latency', linewidth=linewidth)
    pyplot.plot(xData, deadline_line, 'r-', label='Deadline', linewidth=linewidth)
    # pyplot.xlabel('Timestamp')
    pyplot.ylabel('miliseconds',fontsize=labelFontSize)
    pyplot.title('Average p95th delay',fontsize=titleFontSize)
    set_ticks_size(pyplot.gca(),tickFontSize)

    # Plot bottom graph with the energy consumption in mappingInterval
    pyplot.subplot(3, 1, 2)
    xData = range(len(power))
    pyplot.plot(xData, power, 'b-', label='Energy Consumption', linewidth=linewidth)
    # pyplot.xlabel('Mapping Interval')
    pyplot.ylabel('Watts',fontsize=labelFontSize)
    pyplot.title('Power',fontsize=titleFontSize)
    set_ticks_size(pyplot.gca(),tickFontSize)

    # Plot bottom graph with the energy consumption in mappingInterval
    pyplot.subplot(3, 1, 3)
    # xData = [i for i in range(len(mappingData))]
    mappings = [CORE_MAPPING[str(i)] for i in mappingData]
    xData = range(len(mappings))
    pyplot.plot(xData, mappings, ':*', label='Core Mapping', linewidth=linewidth)
    # pyplot.xlabel('Mapping Interval')
    pyplot.ylabel('Core',fontsize=labelFontSize)
    pyplot.title('Core Mapping',fontsize=titleFontSize)

    y = ['1 small', '2 small', '3 small', '4 small', '1 big', '2 big', '3 big', '4 big']
    # labels = []
    # ind = 0
    # for (tick, label) in zip(pyplot.get_xticks(), pyplot.get_xticklabels()):
    #     print tick
    #     if str(tick).split('.')[1] == '0':
    #         labels.append(y[ind])
    #         ind += 1
    #     else:
    #         labels.append('0')
    #         label.set_visible(False)
    pyplot.yticks(range(len(y)),y)

    set_ticks_size(pyplot.gca(),tickFontSize)

    # pyplot.tight_layout() # Only works with python 2.7
    pyplot.subplots_adjust(hspace=0.5)

    # pyplot.show(block=False)
    pyplot.savefig(folder+'/'+parameters+'.png')


def set_ticks_size(gca,tickFontSize):
    """
    Set ticks size in the plots

    :param gca:
    :param tickFontSize:
    :return:
    """

    for tick in gca.xaxis.get_major_ticks():
        tick.label1.set_fontsize(tickFontSize)

    for tick in gca.yaxis.get_major_ticks():
        tick.label1.set_fontsize(tickFontSize)

def evaluate_experiment(exp_log, deadline, folder, base_log = None, normalized = False):
    """
    Computes the following statistics for an experiment:
      - total execution power x total energy
      - total number of violations x total energy
      - MAPE (Mean Average Percetage Error

    :param exp_log: Heartbeats' log file for the current experiment
    :param base_log: Heartbeats' log for the baseline experiment
    :param normalized: Flag indicating whether the results should be normalized to the baseline
    :return: None
    """

    execution_energy = compute_execution_x_energy(exp_log, base_log, normalized)
    violation_energy = compute_violation_x_energy(exp_log, deadline, base_log, normalized)
    mape = compute_MAPE(exp_log, deadline, base_log, normalized)
    f = open(folder+'/evaluation.txt','w')
    if normalized:
        print("Results normalized to {}".format(base_log))
        print("Normalized total execution time x normalized total power consumption:  ",execution_energy)
        print("Normalized total violations x normalized total power consumption:  ",violation_energy)
        print("Normalized MAPE:  ",mape)
        f.write('Results normalized to {}\n'.format(base_log))
    else:
        f.write('Results not normalized\n')

    f.write("Total Time x Total Energy:  "+str(execution_energy)+"\n")
    f.write("Total Violation x Total Energy:  "+str(violation_energy)+"\n")
    f.write("MAPE:  "+str(mape)+"\n")

    f.close()


def compute_execution_x_energy(experiment_log, baseline_log, normalized = True):
    """
    Computes the normalized total execution time x normalized energy consumption
    The values are normalized to the baseline execution (4 big cores).

    The smaller the result, the better is the experiment
    """

    # Load the logs with only the relevant information
    exp_log = pd.read_table(experiment_log, usecols=['Timestamp','total_energy'])
    base_log = pd.read_table(baseline_log, usecols=['Timestamp','total_energy'])

    # Compute the total execution time of the experiment and the baseline
    exp_exec_time  = (exp_log.iloc[-1]['Timestamp'] - exp_log.iloc[0]['Timestamp'])/(10**9)
    base_exec_time = (base_log.iloc[-1]['Timestamp'] - base_log.iloc[0]['Timestamp'])/(10**9)

    # Get the total energy consumtion of the experiment and the baseline
    # It is simply the last line of the 'total_energy' column of the dataframe
    exp_energy  = exp_log.iloc[-1]['total_energy']
    base_energy = base_log.iloc[-1]['total_energy']

    if not normalized:
        return exp_exec_time * exp_energy

    normalized_energy = float(exp_energy)/base_energy
    normalized_exec_time = float(exp_exec_time)/base_exec_time

    return normalized_energy * normalized_exec_time


def compute_violation_x_energy(experiment_log, deadline, baseline_log, normalized = True):
    """
    Computes the normalized total number of deadline violations x normalized energy consumption
    The values are normalized to the baseline execution (4 big cores).

    The smaller the result, the better is the experiment

    deadline should be provided in milisseconds
    """

    # Convert the deadline from ms to ns
    deadline = deadline*(10**6)

    # Load the logs with only the relevant information
    exp_log = pd.read_table(experiment_log, usecols=['Instant_Latency','total_energy'])
    base_log = pd.read_table(baseline_log, usecols=['Instant_Latency','total_energy'])

    # Compute the total number of deadline violations of the experiment and the baseline
    exp_violations  = len(exp_log[exp_log.Instant_Latency > deadline])
    base_violations = len(base_log[base_log.Instant_Latency > deadline])
    if base_violations == 0:
        base_violations = 1
    if exp_violations == 0:
        exp_violations = 1

    if exp_violations == 0:
        exp_violations = 1

    # Get the total energy consumtion of the experiment and the baseline
    # It is simply the last line of the 'total_energy' column of the dataframe
    exp_energy  = exp_log.iloc[-1]['total_energy']
    base_energy = base_log.iloc[-1]['total_energy']

    if not normalized:
        return exp_violations * exp_energy

    normalized_energy = float(exp_energy)/base_energy
    normalized_violations = float(exp_violations)/base_violations
    return normalized_energy * normalized_violations


def compute_MAPE(experiment_log, deadline, baseline_log, normalized = True):
    """
    Computes the normalized MAPE (Mean Average Percentage Error)
    The resulting MAPE is normalized to the baseline execution (4 big cores).

    The smaller the result, the better is the experiment

    deadline should be provided in milisseconds
    """

    # Convert the deadline from ms to ns
    deadline = deadline*(10**6)

    # Load the logs with only the relevant information
    exp_log = pd.read_table(experiment_log, usecols=['Instant_Latency','total_energy'])
    base_log = pd.read_table(baseline_log, usecols=['Instant_Latency','total_energy'])

    # Only keep the information about jobs that violated the deadline
    exp_log  = exp_log[exp_log.Instant_Latency > deadline]
    base_log = base_log[base_log.Instant_Latency > deadline]

    # Sum the tardiness of all jobs
    exp_total_tardiness = ((exp_log['Instant_Latency'] - deadline)/deadline).sum()
    base_total_tardiness = ((base_log['Instant_Latency'] - deadline)/deadline).sum()

    if len(exp_log) == 0:
        exp_MAPE = 1
    else:
        exp_MAPE = 100*(float(1)/len(exp_log)*exp_total_tardiness)

    if not normalized:
        return exp_MAPE
    print('Exp_MAPE: ',exp_MAPE)

    if len(base_log) == 0:
        base_MAPE = 1
    else:
        base_MAPE = 100*(float(1)/len(base_log)*base_total_tardiness)

    print('Base_MAPE:  ',base_MAPE)
    return float(exp_MAPE)/base_MAPE

def violation_ratio(violations,df,folder):
    """
    Print and stores a summary of the experiment in terms of:
    - Total Violations
    - Total Jobs (heartbeats)
    - Violation ratio

    :param violations: a list of the number of violations in each mapping interval
    :param df: Pandas DataFrame containing the logged data
    :param folder: path to folder where plots and summary data should be stored
    :return: Nothing
    """

    total_violations = sum(violations)
    total_jobs = len(df)

    print("Total Violations:  ",total_violations)
    print("Total Jobs:  ",total_jobs)

    print("Violation Ratio:  ",(float(total_violations)/float(total_jobs))*100.0)

    f = open(folder+'/summary.txt','w')
    f.write("Total Violations:  "+str(total_violations)+"\n")
    f.write("Total Jobs:  "+str(total_jobs)+"\n")
    f.write("Violation Ratio:  "+str((float(total_violations)/float(total_jobs))*100.0)+"%"+"\n")

    f.close()


def energy_spent(power,folder):
    """
    Calculates to total energy spent in the experiment
    :param power: A list with the total power consumption in each mapping interval
    :param folder: path to folder where plots and summary data should be stored
    :return: Nothing
    """

    f = open(folder+'/summary.txt','a')
    f.write("Energy Spent:  "+str(sum(power)))
    f.close()


def main():
    description_txt = """plot the:
                        - # of violations;
                        - avg. tardiness;
                        - power consumption.
                        Later on, you may use the mergePlots.py to merge
                        two plots generated by this script and compare the
                        results.
                        """
    parser = argparse.ArgumentParser(description=description_txt)

    parser.add_argument('--logFile', type=str,
                        help='log file generated by the server application\n \
                        (e.g. ../results/hetero-results-${data}/${policy}-${interval}-${deadline}-${bench}-${up}-${down}-${fil}-${spiky}/localhost_access_log.${cur_date}.txt)')

    parser.add_argument('--heteroMapper', type=str,
                        help='log file (hetero-mapper.txt) generated by the octopus-man scheduler\n \
                        (e.g. ../results/hetero-results-${data}/${policy}-${interval}-${deadline}-${bench}-${up}-${down}-${fil}-${spiky}/hetero-mapper.txt)')

    parser.add_argument('--deadline', default = 100, type=int,
                        help='deadline used in the experiment to be analyzed (default: %(default)s)')

    parser.add_argument('--interval', default = 5, type=int,
                        help='mapping interval used in the experiment (default: %(default)s)')

    parser.add_argument('--outputFolder', type=str,
                        help='folder in which the results will be placed\n \
                        (e.g. ../results/hetero-results-${data}/${policy}-${interval}-${deadline}-${bench}-${up}-${down}-${fil}-${spiky})')

    parser.add_argument('--experimentParameters', type=str,
                        help='parameters used in the experiment\n \
                        (${policy}-${interval}-${deadline}-${bench}-${fil}-${spiky})')

    args = parser.parse_args()

    initialize(args)



def initialize(args):
    """
    Initialize the analysis

    :param args: Command line arguments
    :return: Nothing
    """

    log_file = args.logFile
    hetero_mapper = args.heteroMapper
    deadline = args.deadline
    interval = args.interval
    folder = args.outputFolder
    parameters = args.experimentParameters

    print('Log File:  ',log_file)
    print('Hetero Mapper:  ',hetero_mapper)
    print('Folder:  ',folder)
    print('Parameters:  ',parameters)

    df = parse_log_file(log_file) # 'df' is DataFrame
    mapping_data = parse_mapping_data(hetero_mapper) # 'mapping_Data' is a list of core mappings

    violations = count_violations(df,deadline, interval) # 'df' is a dataframe

    avg_latency = average_latency(df,interval) # 'tardiness' is a dictionary

    power = power_consumption(df,interval)

    violation_ratio(violations,df,folder)

    energy_spent(power,folder)

    make_plots(violations,avg_latency,power,mapping_data,folder,parameters,deadline)

    baseline_log = os.environ['BASELINE_LOG']


    evaluate_experiment(log_file, deadline, folder, baseline_log, normalized=True)


if __name__ == "__main__":
    main()
