Directions for running POET/Heartbeat tests:

#########################
	Heartbeat
#########################
# Set PATH variables (should now be automatically set on startup; just need to verify
# that the environment variables are set using the "echo " command):
	~>$ export LD_LIBRARY_PATH="./lib"
	~>$ export HEARTBEAT_ENABLED_DIR=/tmp

# From Heartbeat framework root directory:
	~>$ make
	~>$ make [install or uninstall]
	~>$ sudo chown -R odroid ./*

# Running built-in tests from Heartbeat framework root directory:
	~>$ make bench-tp	# throughput example test
	~>$ make bench-lat	# latency example test

# Integrating Heartbeat into your own processes:
	For each benchmark, find the outermost loop used to process inputs and insert a call to register a heartbeat
	in that loop. At significant performance points, applications make a call to the HB_heartbeat function to
	indicate a heartbeat.

	PARSEC example heartbeats (provided by Hoffman, et. al. in "Application Heartbeats for Software
	Performance and Health", Table 2):
	---------------------------------------
	Benchmark		Heartbeat Location
	---------------------------------------
	blackscholes	every 25000 options
	bodytrack		every frame
	canneal			every 1875 moves
	dedup			every "chunk"
	facesim			every frame
	ferret			every query
	fluidanimate	every frame
	streamcluster	every 200000 points
	swaptions		every "swaption"
	x264			every frame
	---------------------------------------

	Heartbeat API Functions (provided by Hoffman, et. al. in "Application Heartbeats for Software Performance and Health", Table 2):
	--------------------------------------------------------------------------------------------------------------------------------
	name				args							description
	--------------------------------------------------------------------------------------------------------------------------------
	HB_initialize		window[int], local[bool]		Initialize the Heartbeat system and specify sliding window size for
														calculating average heart rate

	HB_heartbeat		tag[int], local[bool]			Generate a heartbeat to indicate progress

	HB_current_rate		window[int], local[bool]		Returns the average heart rate calculated from the last window heartbeats

	HB_set_target_rate	min[int], max[int], local[bool] Called by the application to tell an external observer desired performance
														goals

	HB_get_target_min	local[bool]						Called by the application or an external observer to retrieve the minimum
														target heart rate

	HB_get_target_max	local[bool]						Called by the application or an external observer to retrieve the maximum
														target heart rate

	HB_get_history		n[int], local[bool]				Returns the timestamp, tag, and threat ID for the last n heartbeats
	--------------------------------------------------------------------------------------------------------------------------------


#####################
	POET
#####################
# Set PATH variables
	~>$ export LD_LIBRARY_PATH="./lib"      # set path to POET library
	~>$ export HEARTBEAT_ENABLED_DIR=/tmp   # set path for log files; should be a directory that you have read & write priveledges on

# Setting up and installing POET from POET project root:
	~>$ make
	~>$ make [install or uninstall]

# Running the tests from POET project root directory:
# All example tests can be found in the "/[path_to]/poet/test" or "/[path_to]/poet/bin" directories
# Generally use num_beats=200, target_rate=0, and window_size=20
#   num_beats -- How many times you want the test to run; how many heartbeats you want the program to cycle through before exiting
#   target_rate -- Number of heartbeats per second
#   window_size -- Number of heartbeats used to calculate the heartbeat rate
# Ex: ~>$ ./bin/double_loop_test 200 0 20
	~>$ ./bin/[test_binary] [num_beats] [target_rate] [window_size]

# Run test and output log files for POET and Heartbeat
	~>$ ./bin/[test_binary] [num_beats] [target_rate] [window_size] && cat /tmp/heartbeat_log.txt
	~>$ ./bin/[test_binary] [num_beats] [target_rate] [window_size] && cat /tmp/poet_log.txt

# Or (to just output the log file)
	~>$ cat /tmp/heartbeat_log.txt
	~>$ cat /tmp/poet_log.txt


Heartbeat log of test results will output the following:
    "Beat": Shows what window (or cycle) we are on
    "Tag": Corresponds to "Beat" value
    "Timestamp": UNIX timestamp of the start of the test (can determine how long a "Beat" has taken)
    "Global_Rate":
    "Window_Rate"
    "Instant_Rate"
    "Global_Accuracy"
    "Window_Accuracy"
    "Instant_Accuracy"
    "Global_Power"
    "Window_Power"
    "Instant_Power"
