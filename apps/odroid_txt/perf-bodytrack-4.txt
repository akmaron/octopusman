[PARSEC] Benchmarks to run:  parsec.bodytrack

[PARSEC] [========== Running benchmark parsec.bodytrack [1] ==========]
[PARSEC] Deleting old run directory.
[PARSEC] Setting up run directory.
[PARSEC] Unpacking benchmark input 'simlarge'.
sequenceB_4/
sequenceB_4/BodyShapeParameters.txt
sequenceB_4/CALIB/
sequenceB_4/CALIB/Camera1.cal
sequenceB_4/CALIB/Camera2.cal
sequenceB_4/CALIB/Camera3.cal
sequenceB_4/CALIB/Camera4.cal
sequenceB_4/CAM1/
sequenceB_4/CAM1/image0000.bmp
sequenceB_4/CAM1/image0001.bmp
sequenceB_4/CAM1/image0002.bmp
sequenceB_4/CAM1/image0003.bmp
sequenceB_4/CAM2/
sequenceB_4/CAM2/image0000.bmp
sequenceB_4/CAM2/image0001.bmp
sequenceB_4/CAM2/image0002.bmp
sequenceB_4/CAM2/image0003.bmp
sequenceB_4/CAM3/
sequenceB_4/CAM3/image0000.bmp
sequenceB_4/CAM3/image0001.bmp
sequenceB_4/CAM3/image0002.bmp
sequenceB_4/CAM3/image0003.bmp
sequenceB_4/CAM4/
sequenceB_4/CAM4/image0000.bmp
sequenceB_4/CAM4/image0001.bmp
sequenceB_4/CAM4/image0002.bmp
sequenceB_4/CAM4/image0003.bmp
sequenceB_4/FG1/
sequenceB_4/FG1/image0000.bmp
sequenceB_4/FG1/image0001.bmp
sequenceB_4/FG1/image0002.bmp
sequenceB_4/FG1/image0003.bmp
sequenceB_4/FG2/
sequenceB_4/FG2/image0000.bmp
sequenceB_4/FG2/image0001.bmp
sequenceB_4/FG2/image0002.bmp
sequenceB_4/FG2/image0003.bmp
sequenceB_4/FG3/
sequenceB_4/FG3/image0000.bmp
sequenceB_4/FG3/image0001.bmp
sequenceB_4/FG3/image0002.bmp
sequenceB_4/FG3/image0003.bmp
sequenceB_4/FG4/
sequenceB_4/FG4/image0000.bmp
sequenceB_4/FG4/image0001.bmp
sequenceB_4/FG4/image0002.bmp
sequenceB_4/FG4/image0003.bmp
sequenceB_4/InitialPose.txt
sequenceB_4/PoseParameters.txt
[PARSEC] Running 'time /home/odroid/parsec-3.0/pkgs/apps/bodytrack/inst/arm-linux.gcc-hooks-poet/bin/bodytrack sequenceB_4 4 4 4000 5 0 1':
[PARSEC] [---------- Beginning of output ----------]
init heartbeat with 0.000000 100.000000 30
/tmp/28665
Allocating log for 28665, 57330
Initialized energy reading from: ODROID-XU+E Power Sensors (A15, A7, MEM, GPU)
heartbeat init'd
PARSEC Benchmark Suite Version 3.0-beta-20150206
[HOOKS] PARSEC Hooks Version 1.2
Threading with Posix Threads
Number of threads : 1
Using dataset : sequenceB_4/
4000 particles with 5 annealing layers

[HOOKS] Entering ROI
Processing frame 0
Processing frame 1
Processing frame 2
Processing frame 3
[HOOKS] Leaving ROI

Instant heart rate: 0.640850
Current heart rate: 0.652790
Global heart rate: 0.870386
Finished energy reading from: ODROID-XU+E Power Sensors (A15, A7, MEM, GPU)
heartbeat finished
[HOOKS] Total time spent in ROI: 6.161s
[HOOKS] Terminating

real	0m6.634s
user	0m6.440s
sys	0m0.185s
[PARSEC] [----------    End of output    ----------]
[PARSEC]
[PARSEC] BIBLIOGRAPHY
[PARSEC]
[PARSEC] [1] Bienia. Benchmarking Modern Multiprocessors. Ph.D. Thesis, 2011.
[PARSEC]
[PARSEC] Done.

Energy (A15): 22.079318