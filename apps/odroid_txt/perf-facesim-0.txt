[PARSEC] Benchmarks to run:  parsec.facesim

[PARSEC] [========== Running benchmark parsec.facesim [1] ==========]
[PARSEC] Deleting old run directory.
[PARSEC] Setting up run directory.
[PARSEC] Unpacking benchmark input 'simlarge'.
Face_Data/
Face_Data/Eftychis_840k/
Face_Data/Eftychis_840k/Front_370k/
Face_Data/Eftychis_840k/Front_370k/attachment_list.txt
Face_Data/Eftychis_840k/Front_370k/buccinator_left.constitutive_data
Face_Data/Eftychis_840k/Front_370k/buccinator_right.constitutive_data
Face_Data/Eftychis_840k/Front_370k/corrugator_supercilii_left.constitutive_data
Face_Data/Eftychis_840k/Front_370k/corrugator_supercilii_right.constitutive_data
Face_Data/Eftychis_840k/Front_370k/cranium.attached_nodes
Face_Data/Eftychis_840k/Front_370k/cranium_extended.attached_nodes
Face_Data/Eftychis_840k/Front_370k/depressor_anguli_oris_left.constitutive_data
Face_Data/Eftychis_840k/Front_370k/depressor_anguli_oris_right.constitutive_data
Face_Data/Eftychis_840k/Front_370k/depressor_labii_inferioris_left.constitutive_data
Face_Data/Eftychis_840k/Front_370k/depressor_labii_inferioris_right.constitutive_data
Face_Data/Eftychis_840k/Front_370k/face_simulation.tet
Face_Data/Eftychis_840k/Front_370k/face_simulation_1.tet
Face_Data/Eftychis_840k/Front_370k/face_simulation_128.tet
Face_Data/Eftychis_840k/Front_370k/face_simulation_16.tet
Face_Data/Eftychis_840k/Front_370k/face_simulation_2.tet
Face_Data/Eftychis_840k/Front_370k/face_simulation_3.tet
Face_Data/Eftychis_840k/Front_370k/face_simulation_32.tet
Face_Data/Eftychis_840k/Front_370k/face_simulation_4.tet
Face_Data/Eftychis_840k/Front_370k/face_simulation_6.tet
Face_Data/Eftychis_840k/Front_370k/face_simulation_64.tet
Face_Data/Eftychis_840k/Front_370k/face_simulation_8.tet
Face_Data/Eftychis_840k/Front_370k/face_simulation_original.log
Face_Data/Eftychis_840k/Front_370k/flesh.attached_nodes
Face_Data/Eftychis_840k/Front_370k/flesh_extended.attached_nodes
Face_Data/Eftychis_840k/Front_370k/jaw.attached_nodes
Face_Data/Eftychis_840k/Front_370k/jaw_joint_parameters
Face_Data/Eftychis_840k/Front_370k/levator_anguli_oris_left.constitutive_data
Face_Data/Eftychis_840k/Front_370k/levator_anguli_oris_right.constitutive_data
Face_Data/Eftychis_840k/Front_370k/levator_labii_superioris_left.constitutive_data
Face_Data/Eftychis_840k/Front_370k/levator_labii_superioris_right.constitutive_data
Face_Data/Eftychis_840k/Front_370k/llsan_lateral_left.constitutive_data
Face_Data/Eftychis_840k/Front_370k/llsan_lateral_right.constitutive_data
Face_Data/Eftychis_840k/Front_370k/llsan_medial_left.constitutive_data
Face_Data/Eftychis_840k/Front_370k/llsan_medial_right.constitutive_data
Face_Data/Eftychis_840k/Front_370k/mentalis_left.constitutive_data
Face_Data/Eftychis_840k/Front_370k/mentalis_right.constitutive_data
Face_Data/Eftychis_840k/Front_370k/muscle_list.txt
Face_Data/Eftychis_840k/Front_370k/nasalis_alar_left.constitutive_data
Face_Data/Eftychis_840k/Front_370k/nasalis_alar_right.constitutive_data
Face_Data/Eftychis_840k/Front_370k/nasalis_transverse.constitutive_data
Face_Data/Eftychis_840k/Front_370k/node_divisions_1.dat
Face_Data/Eftychis_840k/Front_370k/node_divisions_128.dat
Face_Data/Eftychis_840k/Front_370k/node_divisions_16.dat
Face_Data/Eftychis_840k/Front_370k/node_divisions_2.dat
Face_Data/Eftychis_840k/Front_370k/node_divisions_3.dat
Face_Data/Eftychis_840k/Front_370k/node_divisions_32.dat
Face_Data/Eftychis_840k/Front_370k/node_divisions_4.dat
Face_Data/Eftychis_840k/Front_370k/node_divisions_6.dat
Face_Data/Eftychis_840k/Front_370k/node_divisions_64.dat
Face_Data/Eftychis_840k/Front_370k/node_divisions_8.dat
Face_Data/Eftychis_840k/Front_370k/orbicularis_oculi_left.constitutive_data
Face_Data/Eftychis_840k/Front_370k/orbicularis_oculi_right.constitutive_data
Face_Data/Eftychis_840k/Front_370k/orbicularis_oris.constitutive_data
Face_Data/Eftychis_840k/Front_370k/particle_positions_1.dat
Face_Data/Eftychis_840k/Front_370k/particle_positions_128.dat
Face_Data/Eftychis_840k/Front_370k/particle_positions_16.dat
Face_Data/Eftychis_840k/Front_370k/particle_positions_2.dat
Face_Data/Eftychis_840k/Front_370k/particle_positions_3.dat
Face_Data/Eftychis_840k/Front_370k/particle_positions_32.dat
Face_Data/Eftychis_840k/Front_370k/particle_positions_4.dat
Face_Data/Eftychis_840k/Front_370k/particle_positions_6.dat
Face_Data/Eftychis_840k/Front_370k/particle_positions_64.dat
Face_Data/Eftychis_840k/Front_370k/particle_positions_8.dat
Face_Data/Eftychis_840k/Front_370k/peak_isometric_stress_list.txt
Face_Data/Eftychis_840k/Front_370k/procerus_left.constitutive_data
Face_Data/Eftychis_840k/Front_370k/procerus_right.constitutive_data
Face_Data/Eftychis_840k/Front_370k/risorius_left.constitutive_data
Face_Data/Eftychis_840k/Front_370k/risorius_right.constitutive_data
Face_Data/Eftychis_840k/Front_370k/zygomatic_major_left.constitutive_data
Face_Data/Eftychis_840k/Front_370k/zygomatic_major_right.constitutive_data
Face_Data/Eftychis_840k/Front_370k/zygomatic_minor_left.constitutive_data
Face_Data/Eftychis_840k/Front_370k/zygomatic_minor_right.constitutive_data
Face_Data/Eftychis_840k/eftychis_cranium_collision_surface.phi
Face_Data/Eftychis_840k/eftychis_cranium_collision_surface.rgd
Face_Data/Eftychis_840k/eftychis_cranium_collision_surface.tri
Face_Data/Eftychis_840k/eftychis_cranium_collision_surface_smoothed.phi
Face_Data/Eftychis_840k/eftychis_cranium_collision_surface_smoothed.rgd
Face_Data/Eftychis_840k/eftychis_cranium_collision_surface_smoothed.tri
Face_Data/Eftychis_840k/eftychis_jaw_collision_surface.phi
Face_Data/Eftychis_840k/eftychis_jaw_collision_surface.rgd
Face_Data/Eftychis_840k/eftychis_jaw_collision_surface.tri
Face_Data/Eftychis_840k/eftychis_jaw_collision_surface_smoothed.phi
Face_Data/Eftychis_840k/eftychis_jaw_collision_surface_smoothed.rgd
Face_Data/Eftychis_840k/eftychis_jaw_collision_surface_smoothed.tri
Face_Data/Motion_Data/
Face_Data/Motion_Data/Storytelling_Controls/
Face_Data/Motion_Data/Storytelling_Controls/storytelling_controls.1
Face_Data/Motion_Data/Storytelling_Controls/storytelling_controls.2
[PARSEC] Running 'time /home/odroid/parsec-3.0/pkgs/apps/facesim/inst/arm-linux.gcc-hooks-poet/bin/facesim -timing -threads 1':
[PARSEC] [---------- Beginning of output ----------]
PARSEC Benchmark Suite Version 3.0-beta-20150206
[HOOKS] PARSEC Hooks Version 1.2
Creating directory using system("mkdir -p Storytelling/output")...Successful!
Simulation                                        init heartbeat with 0.000000 100.000000 30
/tmp/30083
Allocating log for 30083, 60166
Initialized energy reading from: ODROID-XU+E Power Sensors (A15, A7, MEM, GPU)
heartbeat init'd
Reading simulation model : ./Face_Data/Eftychis_840k/Front_370k/face_simulation_1.tet
Total particles = 80598
Total tetrahedra = 372126
muscles = 32
attachments = 3
[HOOKS] Entering ROI

  Frame 1                                         
  END Frame 1                                      43.4649 s[HOOKS] Leaving ROI

SIMULATION                                          0.0000
  FRAME                                            43.4649
    ADB                                            43.4621
      UPBS                                          4.9616
        UPBS (FEM) - Initialize                     0.4997 s
        UPBS (FEM) - Element Loop                   4.4614 s
        UPBS (CPF)                                  0.0000 s
      ADO                                          38.5003
        ADO - Update collision bodies               0.0000 s
        AOTSQ                                      38.5001
          AOTSQ - Initialize                        0.0025 s
          AOTSQ - NR loop                          38.4974
            AOTSQ - NR loop - Initialize            0.0047 s
            UCPF                                    0.3460 s
            NRS                                    32.1231
              NRS - Initialize                      0.0000 s
              NRS - Boundary conditions 1           0.0027 s
              UPBS                                  5.4351
                UPBS (FEM) - Initialize             0.0000 s
                UPBS (FEM) - Element Loop           5.4347 s
                UPBS (CPF)                          0.0000 s
              AFD (FEM)                             0.1034 s
              AVIF                                  0.5403
                AVIF (FEM)                          0.5361 s
                AVIF (CPF)                          0.0039 s
              NRS - Boundary conditions 2           0.0008 s
              NRS - Compute residual                0.0046
              NRS - Copy initial guess              0.0079 s
            UPBS                                    5.4622
              UPBS (FEM) - Initialize               0.0000 s
              UPBS (FEM) - Element Loop             5.4604 s
              UPBS (CPF)                            0.0000 s
            AVIF                                    0.5506
              AVIF (FEM)                            0.5464 s
              AVIF (CPF)                            0.0038 s
            AOTSQ - NR loop - Boundary conditions   0.0008 s
            AOTSQ - NR loop - Compute residual      0.0033 s

Instant heart rate: 0.000000
Current heart rate: 0.000000
Global heart rate: 0.000000
Finished energy reading from: ODROID-XU+E Power Sensors (A15, A7, MEM, GPU)
heartbeat finished
[HOOKS] Total time spent in ROI: 43.466s
[HOOKS] Terminating

real	0m55.893s
user	0m52.935s
sys	0m2.445s
[PARSEC] [----------    End of output    ----------]
[PARSEC]
[PARSEC] BIBLIOGRAPHY
[PARSEC]
[PARSEC] [1] Bienia. Benchmarking Modern Multiprocessors. Ph.D. Thesis, 2011.
[PARSEC]
[PARSEC] Done.

Energy (A7): 18.691757