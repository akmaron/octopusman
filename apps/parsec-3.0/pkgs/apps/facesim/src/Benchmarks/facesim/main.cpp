//#####################################################################
// Copyright 2004, Igor Neverov, Eftychios Sifakis.
// This file is part of PhysBAM whose distribution is governed by the license contained in the accompanying file PHYSBAM_COPYRIGHT.txt.
//#####################################################################
#include "../../Public_Library/Utilities/PARSE_ARGS.h"
#include "../../Public_Library/Thread_Utilities/THREAD_POOL.h"
#include "../../Public_Library/Thread_Utilities/THREAD_DIVISION_PARAMETERS.h"

#include "FACE_DRIVER.h"
#include "Storytelling/STORYTELLING_EXAMPLE.h"
#include "../../Public_Library/Utilities/LOG.h"

#ifdef ENABLE_PARSEC_HOOKS
#include <hooks.h>
#endif

#define HB_ENERGY_IMPL
#include <heartbeats/hb-energy.h>
#include <heartbeats/heartbeat-accuracy-power.h>
#include <poet/poet.h>
#include <poet/poet_config.h>

#define PREFIX "FACESIM"
#define USE_POET // Power and performance control

heartbeat_t* heart;
poet_state* state;
static poet_control_state_t* control_states;
static poet_cpu_state_t* cpu_states;

using namespace PhysBAM;

#ifdef ENABLE_PTHREADS
//Use serial code
bool PHYSBAM_THREADED_RUN = true;
# else
//Use multi-threaded code
bool PHYSBAM_THREADED_RUN = false;
#endif //ENABLE_PTHREADS

static inline void hb_poet_init() {
    float min_heartrate;
    float max_heartrate;
    int window_size;
    //Qihang
    int buffer_depth;
    char const * log = "/home/qihang/log/facesim.log";
    double power_target;
    unsigned int nstates;

    if(getenv(PREFIX"_MIN_HEART_RATE") == NULL) {
      min_heartrate = 0.0;
    } else {
      min_heartrate = atof(getenv(PREFIX"_MIN_HEART_RATE"));
    }
    if(getenv(PREFIX"_MAX_HEART_RATE") == NULL) {
      max_heartrate = 100.0;
    } else {
      max_heartrate = atof(getenv(PREFIX"_MAX_HEART_RATE"));
    }
    if(getenv(PREFIX"_WINDOW_SIZE") == NULL) {
      window_size = 30;
    } else {
      window_size = atoi(getenv(PREFIX"_WINDOW_SIZE"));
    }
    //Qihang
    if(getenv("BUFFER_DEPTH") == NULL) {
      buffer_depth = 100;
    } else {
      buffer_depth = atoi(getenv("BUFFER_DEPTH"));
    }

    if(getenv(PREFIX"_POWER_TARGET") == NULL) {
      power_target = 70;
    } else {
      power_target = atof(getenv(PREFIX"_POWER_TARGET"));
    }

    printf("init heartbeat with %f %f %d\n", min_heartrate, max_heartrate, window_size);
    heart = heartbeat_acc_pow_init(window_size, buffer_depth, log,
                                   min_heartrate, max_heartrate,
                                   0, 100,
                                   1, hb_energy_impl_alloc(), power_target, power_target);
    if (heart == NULL) {
      fprintf(stderr, "Failed to init heartbeat.\n");
      exit(1);
    }
#ifdef USE_POET
    if (get_control_states(NULL, &control_states, &nstates)) {
      fprintf(stderr, "Failed to load control states.\n");
      exit(1);
    }
    if (get_cpu_states(NULL, &cpu_states, &nstates)) {
      fprintf(stderr, "Failed to load cpu states.\n");
      exit(1);
    }
    state = poet_init(heart, nstates, control_states, cpu_states, &apply_cpu_config, &get_current_cpu_state, 1, "poet.log");
    if (state == NULL) {
      fprintf(stderr, "Failed to init poet.\n");
      exit(1);
    }
#endif
   printf("heartbeat init'd\n");

}

static inline void hb_poet_finish() {
#ifdef USE_POET
    poet_destroy(state);
    free(control_states);
    free(cpu_states);
#endif
    heartbeat_finish(heart);
    printf("heartbeat finished\n");
}

int main (int argc, char* argv[])
{
#ifdef PARSEC_VERSION
#define __PARSEC_STRING(x) #x
#define __PARSEC_XSTRING(x) __PARSEC_STRING(x)
	printf ("PARSEC Benchmark Suite Version "__PARSEC_XSTRING (PARSEC_VERSION) "\n");
	fflush (NULL);
#else
	printf ("PARSEC Benchmark Suite\n");
	fflush (NULL);
#endif //PARSEC_VERSION
#ifdef ENABLE_PARSEC_HOOKS
	__parsec_bench_begin (__parsec_facesim);
#endif

	PARSE_ARGS parse_args;
	parse_args.Add_Integer_Argument ("-restart", 0);
	parse_args.Add_Integer_Argument ("-lastframe", 300);
	parse_args.Add_Integer_Argument ("-threads", 1);
	parse_args.Add_Option_Argument ("-timing");
	parse_args.Parse (argc, argv);

	STORYTELLING_EXAMPLE<float, float> example;

	FILE_UTILITIES::Create_Directory (example.output_directory);
	LOG::Copy_Log_To_File (example.output_directory + "/log.txt", example.restart);

	if (parse_args.Is_Value_Set ("-threads"))
	{
		static char tmp_buf[255];
		sprintf (tmp_buf, "PHYSBAM_THREADS=%d", parse_args.Get_Integer_Value ("-threads"));

		if (putenv (tmp_buf) < 0) perror ("putenv");
	}

	if (parse_args.Is_Value_Set ("-restart"))
	{
		example.restart = true;
		example.restart_frame = parse_args.Get_Integer_Value ("-restart");
	}

	if (parse_args.Is_Value_Set ("-lastframe"))
	{
		example.last_frame = parse_args.Get_Integer_Value ("-lastframe");
	}

	if (parse_args.Is_Value_Set ("-timing"))
	{
		example.write_output_files = false;
		example.verbose = false;
	}

	if (PHYSBAM_THREADED_RUN == false && parse_args.Get_Integer_Value ("-threads") > 1)
	{
		printf ("Error: Number of threads cannot be greater than 1 for serial runs\n");
		exit (1);
	}

	hb_poet_init();

	THREAD_DIVISION_PARAMETERS<float>& parameters = *THREAD_DIVISION_PARAMETERS<float>::Singleton();
	parameters.grid_divisions_3d = VECTOR_3D<int> (5, 5, 5);

	FACE_DRIVER<float, float> driver (example);

	driver.Execute_Main_Program();

	delete (THREAD_POOL::Singleton());

	hb_poet_finish();

#ifdef ENABLE_PARSEC_HOOKS
	__parsec_bench_end();
#endif

	return 0;
}
