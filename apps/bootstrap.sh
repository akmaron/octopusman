#!/bin/bash
#Plase do line3 ~ line5 to execute the scripts
#Download and enterthe experiemnt git repo
#git clone https://github.com/SailC/HETCMP.git ~/hetcmp_octopus
#cd ~/hetcmp_octopus
#./bootstrap.sh

#########Download all the components needed for the experiment##########
./download.sh

##########Set up environment variable before running
source setenv.sh

# by default the poet scheduler is turned off
# ./turn_on_poet

##########Add patch to the source code###################################
./patch.sh

##########Build and install Heartbeats lib and poet
echo ''=====>install heartbeats''
./build_heartbeats.sh
echo ''=====>heartbeats installed ''
echo ''=====>install poet''
./build_poet.sh
echo ''=====>poet installed''

##########Build benchmarks##################
./build_benchmarks

