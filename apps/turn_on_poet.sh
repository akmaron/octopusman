#cp makefiles for parsec
cp config_poet/gcc-hooks-poet.bldconf ~/parsec-3.0/config/gcc-hooks-poet.bldconf
#cp makefiles for parmibench
cp config_poet/Makefile_dijkstra ~/parmibench/Products/Network/Dijkstra/Parallel/Makefile
cp config_poet/Makefile_sha ~/parmibench/Products/Security/sha/Makefile
#cp makefiles for mplayer
cp config_poet/config.mak ~/yoz13/MPlayer-1.2/config.mak
