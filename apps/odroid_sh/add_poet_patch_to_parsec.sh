#! /bin/bash

#decompress the parse tar and add the poet patch to the 
#source code 

tar xzvf ~/parsec-3.0-core.tar.gz -C ~
cd ~/parsec-3.0
git init
git add .
git commit -m "Add PARSEC"
git apply ~/parsec-3.0-beta-20130728-poet.diff
git add .
git commit -m "Add POET"
