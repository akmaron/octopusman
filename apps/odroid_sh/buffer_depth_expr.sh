#!/bin/bash

pushd .

pack='blackscholes'
depth=1
while [ ${depth} -le 1000 ]
do
export BUFFER_DEPTH=${depth}
echo -e ${bldblu}"\t BUFFER_DEPTH set: " ${BUFFER_DEPTH} ${Color_Off}
cd /home/qihang/odroid_sh
parsecmgmt -c gcc-hooks-poet -a run -p ${pack} -i simlarge -n 1 
#parsecmgmt -c gcc-hooks-poet -a run -p ${pack} -i simlarge -n 1 | grep 'real' >> '/home/qihang/log/blackscholes_buffer_depth.log'
#parsecmgmt -c gcc-hooks-poet -a run -p ${pack} -i simlarge -n 1 &> '/home/qihang/log/blackscholes_'${depth}'.txt'

depth=$((${depth} * 10))
done

popd
