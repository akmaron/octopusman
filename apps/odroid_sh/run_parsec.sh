#! /bin/bash
#PACKS=('blackscholes' 'bodytrack' 'facesim' 'ferret' 'x264')
PACKS=('blackscholes')
export BUFFER_DEPTH=1
for pack in ${PACKS[@]}
do
echo $pack, '1 small'
export BLACKSCHOLES_LOG=/home/qihang/log/$pack-0.log
taskset -c 0 parsecmgmt -c gcc-hooks-poet -a run -p ${pack} -i simlarge -n 1

echo $pack, '2 small'
export BLACKSCHOLES_LOG=/home/qihang/log/$pack-0-1.log
taskset -c 0,1 parsecmgmt -c gcc-hooks-poet -a run -p ${pack} -i simlarge -n 2

echo $pack, '3 small'
export BLACKSCHOLES_LOG=/home/qihang/log/$pack-0-1-2.log
taskset -c 0,1,2 parsecmgmt -c gcc-hooks-poet -a run -p ${pack} -i simlarge -n 3

echo $pack, '4 small'
export BLACKSCHOLES_LOG=/home/qihang/log/$pack-0-1-2-3.log
taskset -c 0,1,2,3 parsecmgmt -c gcc-hooks-poet -a run -p ${pack} -i simlarge -n 4

echo $pack, '1 big'
export BLACKSCHOLES_LOG=/home/qihang/log/$pack-4.log
taskset -c 4 parsecmgmt -c gcc-hooks-poet -a run -p ${pack} -i simlarge -n 1

echo $pack, '2 big'
export BLACKSCHOLES_LOG=/home/qihang/log/$pack-4-5.log
taskset -c 4,5 parsecmgmt -c gcc-hooks-poet -a run -p ${pack} -i simlarge -n 2

echo $pack, '3 big'
export BLACKSCHOLES_LOG=/home/qihang/log/$pack-4-5-6.log
taskset -c 4,5,6 parsecmgmt -c gcc-hooks-poet -a run -p ${pack} -i simlarge -n 3

echo $pack, '4 big'
export BLACKSCHOLES_LOG=/home/qihang/log/$pack-4-5-6-7.log
taskset -c 4,5,6,7 parsecmgmt -c gcc-hooks-poet -a run -p ${pack} -i simlarge -n 4
done
