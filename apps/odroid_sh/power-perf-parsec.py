
#
# by vinicius petrucci
#

import subprocess
import threading
import time

class PowerThread(threading.Thread):
     def __init__(self, sensor_path):
         threading.Thread.__init__(self)
         self.sensor_path=sensor_path
         self.power_data=[]
         self.energy = 0.0
         self.running = True

     def run(self):
         while self.running:
           p = float(file(self.sensor_path).read())
           self.energy += p
           time.sleep(1)

apps = ['blackscholes', 'bodytrack', 'facesim', 'ferret', 'x264']

# A7 Nodes
#A7_W=`cat /sys/bus/i2c/drivers/INA231/3-0045/sensor_W`"W"

# A15 Nodes
#A15_W=`cat /sys/bus/i2c/drivers/INA231/3-0040/sensor_W`"W"

# --------- MEMORY DATA ----------- # 
#MEM_W=`cat /sys/bus/i2c/drivers/INA231/3-0041/sensor_W`"W"


for a in apps:

 # 1 small
 print a,'1 small'
 a7_power = PowerThread('/sys/bus/i2c/drivers/INA231/3-0045/sensor_W')
 a7_power.start()
 cmd='taskset -c 0 /home/odroid/parsec-3.0/bin/parsecmgmt -c gcc-hooks-poet -a run -p '+a+' -i simlarge -n 1'
 out = subprocess.check_output(cmd, stderr=subprocess.STDOUT, shell=True)
 a7_power.running = False
 a7_power.join()
 file('/home/odroid/perf-' + a+'-0.txt', 'w').write(out+'\nEnergy (A7): '+str(a7_power.energy))
 
 # 2 small
 print a,'2 small'
 a7_power = PowerThread('/sys/bus/i2c/drivers/INA231/3-0045/sensor_W')
 a7_power.start()
 cmd='taskset -c 0,1 /home/odroid/parsec-3.0/bin/parsecmgmt -c gcc-hooks-poet -a run -p '+a+' -i simlarge -n 2'
 out = subprocess.check_output(cmd, stderr=subprocess.STDOUT, shell=True)
 a7_power.running = False
 a7_power.join()
 file('/home/odroid/perf-' + a+'-0-1.txt', 'w').write(out+'\nEnergy (A7): '+str(a7_power.energy))

 # 3 small
 print a,'3 small'
 a7_power = PowerThread('/sys/bus/i2c/drivers/INA231/3-0045/sensor_W')
 a7_power.start()
 cmd='taskset -c 0,1,2 /home/odroid/parsec-3.0/bin/parsecmgmt -c gcc-hooks-poet -a run -p '+a+' -i simlarge -n 3'
 out = subprocess.check_output(cmd, stderr=subprocess.STDOUT, shell=True)
 a7_power.running = False
 a7_power.join()
 file('/home/odroid/perf-' + a+'-0-1-2.txt', 'w').write(out+'\nEnergy (A7): '+str(a7_power.energy))

 # 4 small
 print a,'4 small'
 a7_power = PowerThread('/sys/bus/i2c/drivers/INA231/3-0045/sensor_W')
 a7_power.start()
 cmd='taskset -c 0,1,2,3 /home/odroid/parsec-3.0/bin/parsecmgmt -c gcc-hooks-poet -a run -p '+a+' -i simlarge -n 4'
 out = subprocess.check_output(cmd, stderr=subprocess.STDOUT, shell=True)
 a7_power.running = False
 a7_power.join()
 file('/home/odroid/perf-' + a+'-0-1-2-3.txt', 'w').write(out+'\nEnergy (A7): '+str(a7_power.energy))
 
 # 1 big
 print a,'1 big'
 a15_power = PowerThread('/sys/bus/i2c/drivers/INA231/3-0040/sensor_W')
 a15_power.start()
 cmd='taskset -c 4 /home/odroid/parsec-3.0/bin/parsecmgmt -c gcc-hooks-poet -a run -p '+a+' -i simlarge -n 1'
 out = subprocess.check_output(cmd, stderr=subprocess.STDOUT, shell=True)
 a15_power.running = False
 a15_power.join()
 file('/home/odroid/perf-' + a+'-4.txt', 'w').write(out+'\nEnergy (A15): '+str(a15_power.energy))

 # 2 big
 print a,'2 big'
 a15_power = PowerThread('/sys/bus/i2c/drivers/INA231/3-0040/sensor_W')
 a15_power.start()
 cmd='taskset -c 4,5 /home/odroid/parsec-3.0/bin/parsecmgmt -c gcc-hooks-poet -a run -p '+a+' -i simlarge -n 2'
 out = subprocess.check_output(cmd, stderr=subprocess.STDOUT, shell=True)
 a15_power.running = False
 a15_power.join()
 file('/home/odroid/perf-' + a+'-4-5.txt', 'w').write(out+'\nEnergy (A15): '+str(a15_power.energy))

 # 3 big
 print a,'3 big'
 a15_power = PowerThread('/sys/bus/i2c/drivers/INA231/3-0040/sensor_W')
 a15_power.start()
 cmd='taskset -c 4,5,6 /home/odroid/parsec-3.0/bin/parsecmgmt -c gcc-hooks-poet -a run -p '+a+' -i simlarge -n 3'
 out = subprocess.check_output(cmd, stderr=subprocess.STDOUT, shell=True)
 a15_power.running = False
 a15_power.join()
 file('/home/odroid/perf-' + a+'-4-5-6.txt', 'w').write(out+'\nEnergy (A15): '+str(a15_power.energy))

 # 4 big
 print a,'4 big'
 a15_power = PowerThread('/sys/bus/i2c/drivers/INA231/3-0040/sensor_W')
 a15_power.start()
 cmd='taskset -c 4,5,6,7 /home/odroid/parsec-3.0/bin/parsecmgmt -c gcc-hooks-poet -a run -p '+a+' -i simlarge -n 4'
 out = subprocess.check_output(cmd, stderr=subprocess.STDOUT, shell=True)
 a15_power.running = False
 a15_power.join()
 file('/home/odroid/perf-' + a+'-4-5-6-7.txt', 'w').write(out+'\nEnergy (A15): '+str(a15_power.energy))



