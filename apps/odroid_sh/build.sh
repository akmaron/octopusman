#! /bin/bash
pushd .
cd /home/qihang/odroid_sh
echo 'clean, build & test HeartBeats'
echo '+===========================>'
sleep 3
./build_heartbeats.sh
echo 'clean, build & test POET'
echo '+===========================>'
sleep 3
./build_poet.sh
echo 'clean, build & test parsec'
echo '+===========================>'
sleep 3
./build_parsec.sh
popd
