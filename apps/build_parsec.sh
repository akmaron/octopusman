PACKS=('blackscholes' 'bodytrack' 'x264')

for pack in ${PACKS[@]}
do
    parsecmgmt -a uninstall -c gcc-hooks-poet -p ${pack}
    parsecmgmt -a clean -c gcc-hooks-poet -p ${pack}
    parsecmgmt -a build -c gcc-hooks-poet -p ${pack}
    #parsecmgmt -a run -c gcc-hooks-poet -p ${pack} -n test -i 1
    #sleep 5
done
