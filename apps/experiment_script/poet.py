#! /usr/bin/python
import os

def run_parsecs():
    #parsecs = [('blackscholes', 0.164), ('bodytrack', 0.748), ('x264', 1.485)]
    parsecs = [('blackscholes', 0.219), ('bodytrack', 0.895), ('x264', 0.373)]
    os.environ['BUFFER_DEPTH'] = '1'
    for parsec, deadline in parsecs:
        run_parsec(parsec, deadline)

def run_parsec(name, deadline):
    os.environ[name.upper() + '_MAX_HEART_RATE'] = str(deadline)
    os.environ[name.upper() + '_MIN_HEART_RATE'] = str(deadline)
    for thread_num in range(4, 5):
        print("=======thread num ======", thread_num)
	for input_name in ['simsmall', 'simmedium', 'simlarge', 'native']:
	    log_name = (name + "_log").upper()
	    val = "/home/qihang/log/" + name + '-poet-' + input_name + ".log"
	    os.environ[log_name] = val
	    print(log_name, val)
	    cmd = "parsecmgmt -c gcc-hooks-poet -a run -p " + name + " -i " + input_name + " -n " + str(thread_num)
	    print(cmd)
	    os.system(cmd)
	    os.system("./cleanup.sh")

def run_mplayer():
    os.environ['MPLAYER_MAX_HEART_RATE'] = str(16.6)
    os.environ['MPLAYER_MIN_HEART_RATE'] = str(16.6)
    for thread_num in range(4, 5):
        print("=======thread num ======", thread_num)
	for input_name in ['alfa1080p.mp4', 'alfa720p.mp4', 'alfa480p.mp4', 'alfa360p.mp4']:
	    log_name = "MPLAYER_LOG"
	    val = "/home/qihang/log/mplayer-poet-" + input_name +  ".log"
	    os.environ[log_name] = val
	    print(log_name, val)
	    cmd = "/home/qihang/yoz13/MPlayer-1.2/mplayer -lavdopts threads=" + str(thread_num) +" -fps 30 -nosound -correct-pts -vo null -ao null /home/qihang/yoz13/video/" + input_name
	    print(cmd)
	    os.system(cmd)
	    os.system("./cleanup.sh")

def run_parmibench():
    for thread_num in range(4, 5):
        print("=======thread num ======", thread_num)
	    # run dijkstra
	log_name = ("dijkstra_log").upper()
	val = "/home/qihang/log/dijkstra-poet-mqueue" + ".log"
	os.environ[log_name] = val
        os.environ['DIJKSTRA_MAX_HEART_RATE'] = str(1.90)
        os.environ['DIJKSTRA_MIN_HEART_RATE'] = str(1.90)
	print(log_name, val)
	dijPath = "/home/qihang/parmibench/Products/Network/Dijkstra/Parallel/"
        cmd = dijPath + "dijkstra_parallel_mqueue " + dijPath + "input_large.dat > " + dijPath + "output_large_m.dat"
	print(cmd)
	os.system(cmd)
	os.system("./cleanup.sh")
	# run sha
	log_name = ("sha_log").upper()
	val = "/home/qihang/log/sha-poet.log"
	os.environ[log_name] = val
        os.environ['SHA_MAX_HEART_RATE'] = str(2.32)
        os.environ['SHA_MIN_HEART_RATE'] = str(2.32)
	print(log_name, val)
	cmd = "/home/qihang/parmibench/Products/Security/sha/sha -P -4 -1000"
	print(cmd)
	os.system(cmd)
	os.system("./cleanup.sh")

   	
		
#def get_core_mapping_threads(thread_num):
#	"""
#	generate core_mappings for the specified number of threads
#	each core_mapping is represented by a tupe with core index 
#	return a list of core_mapping
#	"""
#	mappings = []
#	for big_num in range(thread_num + 1):
#	    small_num = thread_num - big_num
#	    small_cores = get_core_mappings(small_num, small=True)
#	    big_cores = get_core_mappings(big_num, small=False)
#	    for small_core in small_cores:
#	        for big_core in big_cores:
#		    mappings.append(tuple(list(small_core) + list(big_core)))
#	return mappings

def get_core_mapping_threads(thread_num):
	"""
	generate core_mappings for the specified number of threads
	each core_mapping is represented by a tupe with core index 
	return a list of core_mapping
	"""
	
	return [(0, ), (4, )] if thread_num == 1 else\
               [(0, ), (4, ), (0, 1), (4, 5), (0, 4)] if thread_num == 2 else\
               [(0, ), (4, ), (0, 1, 2), (4, 5, 6)] if thread_num == 3 else\
               [(0, ), (4, ), (0, 1, 2, 3), (4, 5, 6, 7)] if thread_num == 4 else \
	       [(0, ), (4, ), (0, 1, 2, 3, 4), (0, 4, 5, 6, 7)] if thread_num == 5 else\
	       [(0, ), (4, ), (0, 1, 2, 3, 4, 5), (0, 1, 4, 5, 6, 7)] if thread_num == 6 else\
	       [(0, ), (4, ), (0, 1, 2, 3, 4, 5, 6), (0, 1, 2, 4, 5, 6, 7)] if thread_num == 7 else\
	       [(0, ), (4, ), (0, 1, 2, 3, 4, 5, 6, 7)] 

#def get_core_mappings(num_core, small=True):
#	i = 0 if small else 4
#	return [()] if num_core == 0 else \
#               [(i,)] if num_core == 1 else\
#	       [(i,), (i, i + 1)] if num_core == 2 else \
#               [(i,), (i, i + 1, i + 2)] if num_core == 3 else \
#	       [(i,), (i, i + 1, i + 2, i + 3)]

#def get_core_mappings(num_core, small=True):
#	i = 0 if small else 4
#	return [()] if num_core == 0 else \
#               [(i,), (i + 1,), (i + 2,), (i + 3, )] if num_core == 1 else\
#	       [(i, i + 1), (i, i + 2), (i, i + 3), (i + 1, i + 2), (i + 1, i + 3), (i + 2, i + 3)] if num_core == 2 else \
#               [(i, i + 1, i + 2), (i, i + 2, i + 3), (i + 1, i + 2, i + 3)] if num_core == 3 else \
#	       [(i, i + 1, i + 2, i + 3)]

if __name__ == '__main__':
    #run_parsecs() 
    run_mplayer()
    #run_parmibench()
