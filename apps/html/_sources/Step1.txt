.. highlight:: python
    :linenothreshold: 0

Set up the basic environment to run the experiments
=========================================================

Download, build and install the monitor library
-----------------------------------------------

The monitor library we use
~~~~~~~~~~~~~~~~~~~~~~~~~~
* ``POET`` (The Performance with Optimal Energy Toolkit) provides a means to monitor
  and adjust application performance while minimizing energy consumption during
  application runtime.
* The Application ``Heartbeats`` framework provides a simple, standardized way for
  applications to monitor their performance and make that information available
  to external observers. 
* POET utilizes the Heartbeats framework to monitor application performance. Since ``Octopusman`` wants to do the same
  thing, we can apply the POET patch and turn off the POET scheduling itself. In other words, by applying the POET patch
  and turning off the POET scheduler, we can monitor the application performance without adjusting it. It's Octopusman
  who makes the decision of performance adjustment.

Download links
~~~~~~~~~~~~~~
* `POET git repo <https://github.com/libpoet/poet>`_
* `Heartbeats git repo <https://github.com/libheartbeats/heartbeats>`_

Build and install the monitor library
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* run ``<git_repo>/odroid_sh/build_heartbeat.sh`` to build and install the ``Heartbeat`` 
* run ``<git_repo>/odroid_sh/build_poet.sh`` to build and install the ``POET`` 

Download and build the benchmarks
---------------------------------

The benchmarks library we use
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* ``Parsec`` - The Princeton Application Repository for Shared-Memory Computers (PARSEC) is a benchmark suite composed of multithreaded programs. 
* ``Parmibench`` - It is a new and open-source benchmark for multiprocessor based embedded system. It comprises a set of
  parallel implementations for seven classical algorithms that cover different computing features of general-purpose processor. 
* ``Mplayer`` - MPlayer is a movie player which runs on many systems 

Download links
~~~~~~~~~~~~~~
* `Parsec <http://parsec.cs.princeton.edu/download.htm>`_
* `Parmibench <https://code.google.com/p/multiprocessor-benchmark/>`_
* `Mplayer <http://www.mplayerhq.hu/design7/dload.html>`_

Build and install the monitor library
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* run ``<git_repo>/odroid_sh/build_parsec.sh`` to build and install the ``Parsec`` benchmark 
* run ``<git_repo>/odroid_sh/build_parmibench.sh`` to build and install the ``Parmibench`` benchmark 
* run ``<git_repo>/odroid_sh/build_mplayer.sh`` to build and install the ``Mplayer`` benchmark 

Configure the Odroid environment
--------------------------------
* Check ODROID performance sensor settings (value in "enabled" should be "1")::

    sudo -i     # may need to be admin/root
    nano /sys/bus/i2c/drivers/INA231/3-0040/enabled #value inside should be 1
    nano /sys/bus/i2c/drivers/INA231/3-0041/enabled #value inside should be 1
    nano /sys/bus/i2c/drivers/INA231/3-0044/enabled #value inside should be 1
    nano /sys/bus/i2c/drivers/INA231/3-0045/enabled #value inside should be 1

* Check if all code corresponds with new architecture ("odroidxue")

    * Check if the values for the sensors correspond to the values you just set.
    * The file  ``nano /path/to/heartbeats/src/hb-energy-odroidxue.c`` may say "4-004X" instead of "3-004X", change it accordingly if that's the case.

* Check if PARSEC config files correspond to hb-energy-odroidxue:

    * In ``/path/to/parsec-3.0/config/gcc-hooks-poet.bldconf``, the variable "LIBS" should have "-lhb-energy-odroidxue" instead of "-lhb-energy-msr". This document only appears after running the POET diff ``parsec-3.0-beta-20130728``.

* Set the environment variable 

      * run ``<git_repo>/odroid_sh/setenv.sh``::

          #set default Heartbeat log file path
          export HEARTBEAT_ENABLED_DIR=/home/qihang/log
          echo "\tHEARTBEAT_ENABLED_DIR set: "$HEARTBEAT_ENABLED_DIR 

          #Set the environement variables 
          cd /home/qihang/parsec-3.0
          . ./env.sh
          export HEART_BEAT_SRC_DIR=/home/qihang/heartbeats
          export POET_SRC_DIR=/home/qihang/poet

* Other important notes

    * After modifying any of the files (i.e. changes to PARSEC configs, Heartbeats source,etc.), please remember to run the following to apply the changes in this order (PARSEC depends on --> POET depends on --> Heartbeats):
    * in the Heartbeats directory::

        make uninstall
        make clean
        make
        make install

    * Test with:~>$ "make bench-tp"
    * in the POET directory::

        make uninstall
        make clean
        make
        make install

    * Test with:~>$ "./bin/double_loop_test 200 0 20"

    * in the PARSEC directory ("-c" flag indicates the config file to run with the benchmark) ::

        parsec -c gcc-hooks-poet -p [benchmark] -a uninstall
        parsec -c gcc-hooks-poet -p [benchmark] -a clean
        parsec -c gcc-hooks-poet -p [benchmark] -a build

    * Test with:~>$ "./bin/parsecmgmt -c gcc-hooks-poet -p bodytrack -a run -i simmedium"

    * Additionally, you may have to run the "hb_cleanup.sh" script (as root/admin) to remove any Heartbeats shared memory files from time to time, as it does not clean up temporary shared files automatically.
