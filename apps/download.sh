#!/bin/bash
#Download Heartbeats
git clone https://github.com/libheartbeats/heartbeats.git
#Download Poet
git clone https://github.com/libpoet/poet.git
#Download and untar Parsec
wget http://parsec.cs.princeton.edu/download/3.0/parsec-3.0.tar.gz
tar vxzf parsec-3.0.tar.gz
#Download and unzip Parmibench
wget https://multiprocessor-benchmark.googlecode.com/files/Products.zip
unzip Products.zip
#Download and untar mplayer
wget http://www.mplayerhq.hu/MPlayer/releases/MPlayer-1.2.tar.xz
tar vxjf MPlayer-1.2.tar.xz

