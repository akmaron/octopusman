#!/usr/bin/env bash
date=`date +%Y-%m-%d-%H%M`

benchmarks=( 'dijkstra' 'sha' )
deadlines=(  "525" "430" )
baseline_logs=( "dijkstra-4,5,6,7-mqueue" "sha-4,5,6,7-default" )

export HETCOMP="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export BUFFER_DEPTH=1

echo "HETCOMP: " $HETCOMP

for scheduler in 'octopus-man-control'; #'hetero-mapper3-adapt-deadzone2';
do

for workload_metric in 'QoS';# 'QoS';
do 

for workload_behavior in 'default' ;
do

for ((i=0;i<${#benchmarks[@]};++i));
do

benchmark=${benchmarks[i]}
deadline=${deadlines[i]}
baseline=${baseline_logs[i]}

export BASELINE_LOG=results/baselines/4_big/${baseline}.log
export ${benchmark^^}_LOG=/ramcache/logs/heartbeat.log

echo $DIJKSTRA_LOG

for interval in '10' ;
do

for policy in 'reactive' ;
do

for cores in '4,5,6,7' ;
do

for alpha in '0.6';
do

for gamma in '0.95' ;
do


    # Create the results folder
    full_path=results/${scheduler}/${date}/${policy}-${interval}-${deadline}-${workload_behavior}-${alpha}-${gamma}-${workload_metric}-${benchmark}
    mkdir -p $full_path
    mkdir -p /ramcache/logs
    chmod 777 -R /ramcache/logs/

    # start hetero mapper
    echo -n 1 > /ramcache/mapper_running
    taskset -c 5 python3.4 octopusman/control/${scheduler}.py $policy $interval $deadline $benchmark  $workload_behavior $cores $alpha $gamma $workload_metric  &


    # # start the application
    if [ $benchmark = 'dijkstra' ]
        then
            dijPath=apps/parmibench/Products/Network/Dijkstra/Parallel
            cmd="${dijPath}/dijkstra_parallel_mqueue ${dijPath}/input_large.dat > ${dijPath}/output_large_m.dat"
        else
            cmd="apps/parmibench/Products/Security/sha/sha -P -4 -1000"
    fi

    sleep 10
    taskset -c $cores $cmd
    ####

    # # finalize mapper
    echo -n 0 > /ramcache/mapper_running

    sleep $interval

    # Move log files to corresponding results folder
    mv /ramcache/logs/* $full_path/
    rm -Rf /ramcache/*

    #full_path=results/$date/$policy-$interval-$deadline-$workload_behavior-$alpha-$gamma-$workload_metric-$benchmark
    python analyzeLog.py --logFile $full_path/heartbeat.log --heteroMapper $full_path/hetero-mapper.txt --deadline $deadline --outputFolder $full_path --experimentParameters $policy-$interval-$deadline-$workload_behavior-$cores-$alpha-$gamma-$workload_metric-$benchmark

done
done
done
done
done
done
done
done
#mv octopusman/control/Q_table-100-20.save $full_path/Q_table-100-20.save_$workload_metric
#mv octopusman/control/Q_table_updates-100-20.save $full_path/Q_table_updates-100-20.save_$workload_metric
done
