#!/usr/bin/env bash
# Input parameters are
#  date: 2015-04-06
#  time: 1950
date=$1-$2
benchmarks=( "x264" "blackscholes" "bodytrack" )
deadlines=( "2683" "4575" "1117" )
baseline_logs=( "x264-4,5,6,7-native" "blackscholes-4,5,6,7-native" "bodytrack-4,5,6,7-native" )

for scheduler in 'octopus-man-control' ; #'hetero-mapper3-adapt-deadzone2';
do

for workload_metric in 'QoS';# 'QoS';
do

for workload_behavior in 'native' ;
do

for ((i=0;i<${#benchmarks[@]};++i));
do

benchmark=${benchmarks[i]}
deadline=${deadlines[i]}
baseline=${baseline_logs[i]}

export BASELINE_LOG=results/baselines/4_big/${baseline}.log

for interval in '10' ;
do

for policy in 'reactive' ;
do

for cores in '4,5,6,7' ;
do

for alpha in '0.6';
do

for gamma in '0.95' ;
do

full_path=results/${scheduler}/${date}/${policy}-${interval}-${deadline}-${workload_behavior}-${alpha}-${gamma}-${workload_metric}-${benchmark}
python analyzeLog.py --logFile $full_path/heartbeat.log --interval $interval --heteroMapper $full_path/hetero-mapper.txt --deadline $deadline --outputFolder $full_path --experimentParameters $policy-$interval-$deadline-$workload_behavior-$cores-$alpha-$gamma-$workload_metric-$benchmark

done
done
done
done
done
done
done
done
done
