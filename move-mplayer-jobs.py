#! /usr/bin/python
import os 
from operator import itemgetter
import sys

if len(sys.argv) != 2:
    print('usage: ./move-mplayer-jobs.py [core-mapping]')
    exit()
cores = sys.argv[1]

cmd = 'ps aux | grep "MPlayer-1.2/mplayer"'
f = os.popen(cmd)
lines = [line.split() for line in f.readlines()]

lines.sort(key=lambda x: x[2], reverse=True)
pid = lines[0][1]
#only 1 process for a signle parsec application(muliple threads though)
#get cores from the command line
#move parent process to other cores
cmd = 'taskset -c -p ' + cores[1:-1] + ' ' + pid 
os.system(cmd)
#move threads
cmd = 'ls /proc/' + pid + '/task/'
f = os.popen(cmd)
lines = [line.split() for line in f.readlines()]
for l in lines:
    tid = l[0]
    os.system('taskset -c -p ' + cores[1:-1] + ' ' +tid)
