#
#  Task mapper for heterogeneous processor systems
#  Vinicius Petrucci, UCSD
#  2013

import os
import time
import sys
import psutil
import itertools
import numpy as np

from scipy import stats

up_thrs=[0.8]
down_thrs=[0.5, 0.4, 0.3, 0.2]

CORE_MAPPINGS = [[0], [0,1], [0,1,2], [0,1,2,3],
                 [4], [4,5], [4,5,6], [4,5,6,7]]

deadzone_list = list(itertools.product(up_thrs, down_thrs))
#random.shuffle(deadzone_list)

MAX_POWER = 7.7

HYST_FACTOR = 3
hyst_count = 0

PARSEC_APPS = ['blackscholes', 'bodytrack', 'x264']

PARMIBENCH_APPS = ['sha', 'dijkstra' ]

if len(sys.argv) < 10:
    print('please specify: policy interval deadline app max_qps core_mapping learning_rate discount_rate workload_metric')
    sys.exit(2)

policy = sys.argv[1]
print('policy = ', policy)

SLEEP_INTERVAL = int(sys.argv[2])
print('SLEEP_INTERVAL = ', SLEEP_INTERVAL)

TARGET_DELAY = int(sys.argv[3])
print('TARGET_DELAY = ', TARGET_DELAY)

APP = str(sys.argv[4])
print('APP = ', APP)

MAX_QPS = str(sys.argv[5])
print('MAX_QPS = ', MAX_QPS)

CORE_MAPPING = str(sys.argv[6])
print('CORE_MAPPING = ', CORE_MAPPING)

ALPHA = float(sys.argv[7])
print('ALPHA = ', ALPHA)

gamma = float(sys.argv[8])
print('GAMMA = ', gamma)

workload_metric = str(sys.argv[9])
print('WORKLOAD METRIC = ', workload_metric)

settling_time = 0
settling_count = 0
is_settling = True

private = os.environ['HETCOMP']

app_alloc = list(map(int,CORE_MAPPING.split(",")))
print("Initial App Alloc: ", app_alloc)

if APP in PARSEC_APPS:
    os.system('python3.4 move-parsec-jobs.py {} {}'.format(APP,str(app_alloc).replace(' ','')))
elif APP in PARMIBENCH_APPS:
    os.system('python3.4 move-parmibench-jobs.py {} {}'.format(APP,str(app_alloc).replace(' ','')))
else:
    os.system('python3.4 move-{}-jobs.py {}'.format(APP,str(app_alloc).replace(' ','')))

mon_file = open('/ramcache/logs/hetero-mapper.txt', 'w')

print("Settling Time... ",settling_time," sec.")
cpu_util = psutil.cpu_percent(interval=SLEEP_INTERVAL, percpu=True)

lastTimestamp = None
newMapping = app_alloc

paused_jobs_per_core = {}
job_paused = -1

big_qos_table = {}
small_qos_table = {}

learning_step = 0
learning_max_step = 45
learning_period_count = 0
learning_period = learning_max_step*3*len(deadzone_list)
learning_index = 0
learning_power = {}
learning_delay = {}

learning_alpha_step = 180
learning_alpha_count = 0

p95th_delay_data = []
learning_deadzone = True
learning_alpha = True

DELAY_UP_THR, DELAY_DOWN_THR = deadzone_list[learning_index]
learning_power[(DELAY_UP_THR, DELAY_DOWN_THR)] = []
learning_delay[(DELAY_UP_THR, DELAY_DOWN_THR)] = []

OPTIMAL_PARAM_FILE = '/ramcache/logs/optimal_param'

from scipy.optimize import leastsq
def best_alpha(qos_data):
    x = range(1, len(qos_data))
    
    def residuals(p, y, x):
       # print p, y, x
        err = []
        prev_val = qos_data[0]
        for i in x:
            y2 = p[0] * qos_data[i] + (1-p[0]) * prev_val
            prev_val = y2     
            err += [y[i] - y2]        
        return err
        
    p0 = [0.5]
    plsq = leastsq(residuals, p0, args=(qos_data, x))

    return min(0.8, float(plsq[0]))
   # return float(plsq[0])

def find_best_param2(learning_power, learning_delay, deadline=100):
    good_updown = []
    f = open(OPTIMAL_PARAM_FILE,'a')
    f.write('-----\n')
    for up, down in deadzone_list:
        energy = sum(learning_power[(up,down)])
        p95th_lat = learning_delay[(up,down)]
        f.write(str(up)+','+ str(down)+'\n')
        f.write('energy: '+str(energy)+'\n')
        f.write('p95th_lat: ' +str(p95th_lat)+'\n')
        f.write('edp: '+ str(p95th_lat * float(energy))+'\n')
        if p95th_lat < TARGET_DELAY*up:
            break
    best_param = (up,down)
    f.write('search found best param: '+ str(best_param)+'\n')
    f.close()
    return best_param

prev_p95th_delay = 0.0
smooth_p95th_delay = 0.0
prev_avg_qps = 0.0

qos_monitor = __import__("monitor-heartbeat")

lastTimestamp = None

while True:

    t_begin = time.time()
    keep_running = int(open('/ramcache/mapper_running').read())
    if not keep_running: break
    
    if learning_alpha:
        learning_alpha_count += 1
        if learning_alpha_count > learning_alpha_step:
          #  learning_deadzone = True
            learning_alpha = False
            ALPHA = best_alpha(p95th_delay_data)
            open(OPTIMAL_PARAM_FILE,'a').write('------\nbest alpha: ' + str(ALPHA)+'\n')
            learning_alpha_count = 0
            #print p95th_delay_data

    if learning_deadzone:

        if learning_index >= len(deadzone_list)-1:
            # pick up the best
            DELAY_UP_THR, DELAY_DOWN_THR = find_best_param2(learning_power, learning_delay)
            print('optimized parameters found', DELAY_UP_THR, DELAY_DOWN_THR)
            learning_deadzone = False          
        else:    
            if learning_step <= learning_max_step:
                learning_step += 1
            else:
                learning_index += 1
                DELAY_UP_THR, DELAY_DOWN_THR = deadzone_list[learning_index]
                learning_power[(DELAY_UP_THR, DELAY_DOWN_THR)] = []
                learning_delay[(DELAY_UP_THR, DELAY_DOWN_THR)] = []
                learning_step = 0
                if APP in PARSEC_APPS:
                    os.system('python3.4 move-parsec-jobs.py {} {}'.format(APP,'[4,5,6,7]'))
                elif APP in PARMIBENCH_APPS:
                    os.system('python3.4 move-parmibench-jobs.py {} {}'.format(APP,'[4,5,6,7]'))
                else:
                    os.system('python3.4 move-{}-jobs.py {}'.format(APP,'[4,5,6,7]'))
                time.sleep(5)
                #learning_alpha = True
    
    learning_period_count += 1
    if learning_period_count > learning_period:
        learning_deadzone = True
        learning_period_count = 0
        learning_step = 0
        learning_index = 0
  #      random.shuffle(deadzone_list)
        open(OPTIMAL_PARAM_FILE,'a').write('------\nWebsearch restarting learning phase, cur alpha: ' + str(ALPHA)+' and cur deadzone:'+str(DELAY_UP_THR)+','+str(DELAY_DOWN_THR)+'\n')
     
    p95th_delay, p95th_power, avg_delay, avg_power, lastTimestamp= qos_monitor.read_values(SLEEP_INTERVAL, lastTimestamp)
    if APP in ['mplayer', 'x264']:
        delay = avg_delay
        power = avg_power
    else:
        delay = p95th_delay
        power = p95th_power

    if workload_metric == 'CPU':
        workload = np.sum([cpu_util[i] for i in app_alloc])/len(app_alloc)
    else:
        workload = (delay / TARGET_DELAY) * 100

    if delay == 0 and power == 0:
        print ('waiting for QoS data from', APP)
        cpu_util = psutil.cpu_percent(interval=SLEEP_INTERVAL, percpu=True)
        continue

    if prev_p95th_delay == 0.0:
        prev_p95th_delay = p95th_delay
        smooth_p95th_delay = p95th_delay
    elif p95th_delay > 0.0:
        p95th_delay = ALPHA * p95th_delay + (1-ALPHA) * prev_p95th_delay
        prev_p95th_delay = p95th_delay
    else:
        time.sleep(SLEEP_INTERVAL)
        continue  

    mon_file.write('%s; %.2f; %.2f; %.2f; %s\n' % (str(cpu_util)[1:-1], workload, delay, power, app_alloc))
    mon_file.flush()
    
    if learning_deadzone:
      
        #learning_power[(DELAY_UP_THR, DELAY_DOWN_THR)] += [total_power]
        learning_delay[(DELAY_UP_THR, DELAY_DOWN_THR)] += p95th_delay
    
    if learning_alpha:
        p95th_delay_data += [p95th_delay]

    if policy == 'reactive':
        if p95th_delay > TARGET_DELAY*DELAY_UP_THR:
            next_mapping_index = CORE_MAPPINGS.index(app_alloc) + 1
            if next_mapping_index < len(CORE_MAPPINGS):
                next_alloc = CORE_MAPPINGS[next_mapping_index]
                print('Moving from {} to {}'.format(app_alloc,next_alloc))
                # Bump it up
                if APP in PARSEC_APPS:
                    os.system('python3.4 move-parsec-jobs.py {} {}'.format(APP,str(next_alloc).replace(' ','')))
                elif APP in PARMIBENCH_APPS:
                    os.system('python3.4 move-parmibench-jobs.py {} {}'.format(APP,str(next_alloc).replace(' ','')))
                else:
                    os.system('python3.4 move-{}-jobs.py {}'.format(APP,str(next_alloc).replace(' ','')))
                app_alloc = next_alloc

            else:
                print('Maintaining maximum core config {}'.format(app_alloc))
            
        elif p95th_delay < TARGET_DELAY*DELAY_DOWN_THR:
            next_mapping_index = CORE_MAPPINGS.index(app_alloc) + 1
            if next_mapping_index >= 0:
                next_alloc = CORE_MAPPINGS[next_mapping_index]
                print('Moving from {} to {}'.format(app_alloc,next_alloc))
                # Bump it down
                if APP in PARSEC_APPS:
                    os.system('python3.4 move-parsec-jobs.py {} {}'.format(APP,str(next_alloc).replace(' ','')))
                elif APP in PARMIBENCH_APPS:
                    os.system('python3.4 move-parmibench-jobs.py {} {}'.format(APP,str(next_alloc).replace(' ','')))
                else:
                    os.system('python3.4 move-{}-jobs.py {}'.format(APP,str(next_alloc).replace(' ','')))
                app_alloc = next_alloc
            else:
                print('Maintaining minimal core cofig {}'.format(app_alloc))
        else:
            print('Maintaining core configuration {}'.format(app_alloc))

    keep_running = int(open('/ramcache/mapper_running').read())
    if not keep_running: break

    t_end = time.time()
    cpu_util = psutil.cpu_percent(interval=SLEEP_INTERVAL-(t_end-t_begin), percpu=True)

mon_file.close()

tail_running = False

time.sleep(2)

#print 'killing perfmon'
#os.system('killall likwid-perfctr')
#os.system('pidof likwid-perfctr')
print('cya!!')


