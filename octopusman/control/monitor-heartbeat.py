#! /usr/bin/python
import os
import numpy as np
log_file_name = "/ramcache/logs/heartbeat.log"
#log_file_name = "heartbeat.log"
    
def read_values(mapping_interval, last_time_stamp):
    #what if file doesn't exist yet
    if not os.path.exists(log_file_name):
        return 0, 0, 0, 0, None

    f = open(log_file_name, 'r')
    lines = f.readlines()
    powers, latencies = [], []

    num_line, i = len(lines), 1
    first_time_stamp = None
    while i <= num_line:
        stats = lines[-i].split()
        if not stats[2].isdigit():
            i += 1
            continue
        time_stamp = float(stats[2])

        # if this is the first time reading the log
        if last_time_stamp is None:
            if first_time_stamp is None:
                first_time_stamp = time_stamp

        # if we are starting a new reading of the log file, we don't want to read past
        # the timestamp indicated by last_time_stamp
        elif first_time_stamp is None:
            if time_stamp <= last_time_stamp:
                return 0, 0, 0, 0, last_time_stamp
            else:
                first_time_stamp = time_stamp


        # if we are continuing a reading in the same interval, read until the last_time_stamp
        # is reached, or until reading all timestamps in the past mapping interval
        if (last_time_stamp is not None and time_stamp <= last_time_stamp): # or ((first_time_stamp - time_stamp) >= mapping_interval*(10 ** 9)):#nano second
            last_time_stamp = first_time_stamp
            print('Read from log file:  ',latencies)
            print('Stopping at delay ',float(stats[-4])/(10**6))
            break

        if last_time_stamp is  None: 
            last_time_stamp = first_time_stamp

        powers.append(float(stats[-5]))
        latencies.append(float(stats[-4])/(10**6))
        i += 1

    #what if powers and latencies are empty
    if not powers or not latencies:
        return 0, 0, 0, 0, last_time_stamp

    power_95th = np.percentile(powers, 95)
    power_avg = np.percentile(powers,50)
    latency_95th = np.percentile(latencies, 95)
    latency_avg = np.percentile(latencies, 50)

    return latency_95th, power_95th, latency_avg, power_avg, last_time_stamp

if __name__ == '__main__':
    last_time_stamp = None

    delay, power, last_time_stamp = read_values(10, last_time_stamp)
    if delay == 0 and power == 0:
        print('waiting')
