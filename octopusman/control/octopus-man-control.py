# -*- coding: utf-8 -*-
import os
import time
import sys
import RL_Deadzone_Adaptor_Simple as RLSimple
import numpy as np
import psutil

HYST_FACTOR = 3
hyst_count = 0

PARSEC_APPS = ['blackscholes', 'bodytrack', 'x264']
PARSEC_POWER = [3, 2.6, 3.75]

PARMIBENCH_APPS = ['sha', 'dijkstra' ]
PARMIBENCH_POWER = [2.5, 3.1]

APP_POWER = {'mplayer': 3.0}

if len(sys.argv) < 10:
    print('please specify: policy interval deadline app max_qps core_mapping learning_rate discount_rate workload_metric')
    sys.exit(2)

policy = sys.argv[1]
print('policy = ', policy)
    
SLEEP_INTERVAL = int(sys.argv[2])
print('SLEEP_INTERVAL = ', SLEEP_INTERVAL)

TARGET_DELAY = int(sys.argv[3])
print('TARGET_DELAY = ', TARGET_DELAY)

APP = str(sys.argv[4])
print('APP = ', APP)

MAX_QPS = str(sys.argv[5])
print('MAX_QPS = ', MAX_QPS)

CORE_MAPPING = str(sys.argv[6])
print('CORE_MAPPING = ', CORE_MAPPING)

alpha = float(sys.argv[7])
print('ALPHA = ', alpha)

gamma = float(sys.argv[8])
print('GAMMA = ', gamma)

workload_metric = str(sys.argv[9])
print('WORKLOAD METRIC = ', workload_metric)

settling_time = 0
settling_count = 0
is_settling = True

## Adriano ##
###########################################################################################
## Scripts for moving the jobs are now in HetComp/apps/APP/scripts/move                  ##
## Everytime a new application is added, it needs to have this folder and a script file  ##
## for moving the job. 'script_location' is used when moving single jobs or batch jobs   ##
###########################################################################################
private = os.environ['HETCOMP']

app_alloc = list(map(int,CORE_MAPPING.split(",")))
print("Initial App Alloc: ", app_alloc)

if APP in PARSEC_APPS:
    os.system('python3.4 move-parsec-jobs.py {} {}'.format(APP,str(app_alloc).replace(' ','')))
elif APP in PARMIBENCH_APPS:
    os.system('python3.4 move-parmibench-jobs.py {} {}'.format(APP,str(app_alloc).replace(' ','')))
else:
    os.system('python3.4 move-{}-jobs.py {}'.format(APP,str(app_alloc).replace(' ','')))

mon_file = open('/ramcache/logs/hetero-mapper.txt', 'w')

prev_p95th_delay = 0.0
prev_avg_qps = 0.0

qos_monitor = __import__("monitor-heartbeat")

if APP in PARSEC_APPS:
    MAX_POWER = PARSEC_POWER[PARSEC_APPS.index(APP)]
elif APP in PARMIBENCH_APPS:
    MAX_POWER = PARMIBENCH_POWER[PARMIBENCH_APPS.index(APP)]
else:
    try:
        MAX_POWER = APP_POWER[APP]
    except KeyError:
        print('No MAX_POWER for {}'.format(APP))
        sys.exit(2) 

rl_adaptor = RLSimple.RL_Deadzone_Adaptor(TARGET_DELAY,MAX_POWER,APP,SLEEP_INTERVAL)
rl_adaptor.SetBinSize(20,"Workload")
if workload_metric == 'QoS':
    max_load = 120
else:
    max_load = 100 # Max load of each socket: 100%

rl_adaptor.SetMaxValue(max_load,"Workload")
rl_adaptor.SetLearningRate(alpha)
rl_adaptor.SetDiscountRate(gamma)
rl_adaptor.CreateQtable()
# rl_adaptor.SetExploration(0.5)

print("Settling Time... ",settling_time," sec.")
cpu_util = psutil.cpu_percent(interval=SLEEP_INTERVAL, percpu=True)

lastTimestamp = None
newMapping = app_alloc

while True:
    keep_running = int(open('/ramcache/mapper_running').read())
    if not keep_running: break 
        
    #
    # read QoS
    #
    t_begin = time.time()

    delay_95th, power_95th, delay_avg, power_avg, lastTimestamp= qos_monitor.read_values(SLEEP_INTERVAL, lastTimestamp)
    if APP in ['mplayer']:
        delay = delay_avg
        power = power_avg
    else:
        delay = delay_95th
        power = power_95th

    if delay == 0 and power == 0:
        print ('waiting for QoS data from', APP)
        cpu_util = psutil.cpu_percent(interval=SLEEP_INTERVAL, percpu=True)
        continue

    if workload_metric == 'CPU':
        workload = np.sum([cpu_util[i] for i in app_alloc])/len(app_alloc)
    else:
        workload = (delay / TARGET_DELAY) * 100

    if policy == 'reactive':
        rl_adaptor.Learn(delay, power, workload, app_alloc)
        newMapping = rl_adaptor.Decide(workload,app_alloc)

    mon_file.write('%s; %.2f; %.2f; %.2f; %s\n' % (str(cpu_util)[1:-1], workload, delay, power, app_alloc))
    mon_file.flush()


    if policy == 'reactive':
        if(app_alloc == newMapping):
            print('Maintaining core configuration: {}'.format(app_alloc))
        else:
            if APP in PARSEC_APPS:
                os.system('python3.4 move-parsec-jobs.py {} {}'.format(APP,str(newMapping).replace(' ','')))
            elif APP in PARMIBENCH_APPS:
                os.system('python3.4 move-parmibench-jobs.py {} {}'.format(APP,str(newMapping).replace(' ','')))
            else:
                os.system('python3.4 move-{}-jobs.py {}'.format(APP,str(newMapping).replace(' ','')))
            app_alloc = newMapping        
    keep_running = int(open('/ramcache/mapper_running').read())
    if not keep_running: break

    t_end = time.time()
    cpu_util = psutil.cpu_percent(interval=SLEEP_INTERVAL-(t_end-t_begin), percpu=True)

mon_file.close()

rl_adaptor.Exit()

time.sleep(2)

print('cya!!')



