################################# RL Deadzone Adaptor ####################################
##                        Extension for the Octopus-Man Scheduler                       ##
##                          Adriano Maron, Vinicius Petrucci                            ##
##                     University of Pittsburgh, University of Michigan                 ##
##########################################################################################

import numpy as np
import collections
import os
import random
import pickle
import multiprocessing as mp

""" Parameters from the applications that are being used during the learning """
PARAMETERS = ["Workload", "Core-Mapping"]

""" All possible actions to be chosen by the learning agent.

    An action corresponds to different core-mappings

    The CORE_MAPPING dictionary mapps the int values to the
    corresponding core-mapping
"""
ACTIONS = [i for i in range(1, mp.cpu_count()+1)]

""" Learning rate """
ALPHA = 0.7

""" Discount rate """
GAMMA = 0

CORE_MAPPING = {"[0]":1,"[0, 1]":2,"[0, 1, 2]":3,"[0, 1, 2, 3]":4,
                "[4]":5,"[4, 5]":6,"[4, 5, 6]":7,"[4, 5, 6, 7]":8}


GLOBAL_PATH = os.environ['HETCOMP']

class RL_Deadzone_Adaptor:
    qtdActions = 0
    qtdStates = 0
    curState = (1,1)
    lastAction = None
    Q_table = None
    Q_table_updates = None
    deadline = 0

    alpha = None
    gamma = None



    prevAvgDelay = 0

    """ Number of bins used during the discretization step.

    Each value corresponds to the number of bins for each parameter in PARAMETERS;
    """
    # bin_number = np.array([1,4])
    bin_number = np.array([1,mp.cpu_count()])
    binSize = [1,1]
    maxValue = [1,mp.cpu_count()]


    """ A state is defined as S_k = (workload,core-mapping).

    After discretization, the attribute 'workload' may assume
    values from the interval given by the corresponding value
    in the array 'bin_number'.

    The values in 'core-mapping' are in the range [1,4].
    """
    states = []

    epsilon = 0.05 # Exploration rate



    def __init__(self,deadline,max_power,app,mapping_interval):
        self.log = True
        self.qtdActions = len(ACTIONS)

        self.delayAverage = collections.deque(maxlen=int(10/int(mapping_interval)))

        self.application = app

        self.curState = None # Smallest workload, 1 small core
        self.lastAction = ACTIONS[0] # Initial action in the mapping [0] by default

        self.deadline = float(deadline)

        self.max_power = float(max_power)

        self.epsilon = 0.05 # Initially, 5% of exploration

        self.violationLog = []
        self.workloadLog = []
        self.rewardLog = []
        self.updateLog = []

    def CreateQtable(self):
        """ Initializes the states and creates a Q-table."""

        self.GenerateStates()

        maxWorkload = str(self.maxValue[PARAMETERS.index("Workload")])
        binSize = str(self.binSize[PARAMETERS.index("Workload")])

        try:
            self.Q_table = np.load(GLOBAL_PATH+'/octopusman/control/Q_table-'+self.application+'-'+maxWorkload+'-'+binSize+'.npy')
        except IOError:
            print("No Q_table found on disk. Create a new one with zeros")
            self.Q_table = np.random.random_sample([self.qtdStates, self.qtdActions])*0.2

        ## Q_table_updates indicates how many times a specific entry of the table was updated.
        try:
            self.Q_table_updates = np.load(GLOBAL_PATH+'/octopusman/control/Q_table_updates-'+self.application+'-'+maxWorkload+'-'+binSize+'.npy')
        except IOError:
            print("No Q_table_updates found on disk. Create a new one with zeros")
            self.Q_table_updates = np.zeros([self.qtdStates, self.qtdActions])

    def R(self, delay, power, core_mapping):
        """ s = ([1,5],[1,4])
            delay = value in ms
            power = float value calculated by the energy model
        """
        # We keep a history of the last 5 delay or 99th delays
        # We are concerned about the average value of those delays
        # over the past 5 mapping intervals
        self.delayAverage.append(delay)

        # If we violated more than 2% of the queries in the previous INTERVAL_MAPPING
        # seconds, give a negative reward for the previous pair (state,action)
        avgDelays = np.average(self.delayAverage)
        reward = 0
        if avgDelays > self.deadline:
            if core_mapping == 8:
                reward = 1
            elif avgDelays < self.prevAvgDelay:
                reward = self.prevAvgDelay/avgDelays
            else:
                reward = -(avgDelays/self.deadline)*(self.max_power/power)
        else:
            reward = (1-(power/self.max_power))

        self.rewardLog.append(reward)
        self.prevAvgDelay = avgDelays
        return reward

    def Q(self, s, a, s_prime, p95th, power):
        # print "S_prime:  ",s_prime
        """ Update the q_values in the Q_table """
        i = self.states.index(s)
        j = ACTIONS.index(a)

        # print "S:  ",s

        i_prime = self.states.index(s_prime)
        q_val = self.Q_table[i_prime][self.maxChoice(self.Q_table[i_prime], self.Q_table_updates[i_prime])]

        newReward =  self.alpha * ( self.R(p95th, power, a) + (self.gamma * q_val) - self.Q_table[i][j] )

        print("Change in the q_Value for action ",a," on state ",s,": ",newReward)

        self.Q_table[i][j] += newReward
        self.Q_table_updates[i][j] += 1

        self.updateLog.append(newReward)

    def Discretize(self, value, parameter):
        """ Discretize receives a continuous value measured corresponding to one of the
        parameters (indicated by the string 'parameter') monitored by the Octopus-Man.
        It outputs its corresponding discretized value"""

        if parameter == "Core-Mapping":
            if type(value) == list: # If the value for core-mapping comes from the octopus as a list [0,1]
                return CORE_MAPPING.get(str(value))
            else: # If the value for core-mapping comes from the simulator as an int 0,1,2 or 3
                return value+1
        if self.log:
            print("Method Discretize ### Binsize:  ",self.binSize[PARAMETERS.index(parameter)])

        discreteVal = 1+int(value/self.binSize[PARAMETERS.index(parameter)])
        maxValue = self.bin_number[PARAMETERS.index(parameter)]
        if discreteVal > maxValue: # The discrete value can assume value of at most the number of bins for the corresponding parameter
            if self.log:
                print('Method Discretize ### Value: {} -- Discretized Max: {}'.format(value ,maxValue))
            return maxValue
        else:
            if self.log:
                print('Method Discretize ### Value: {} -- Discretized Regular: {}'.format(value ,discreteVal))
            return discreteVal

    def ChangeMappings(self, s):
        """ ChangeMappings chooses the best action (core change) for the state `s`."""

        q_vals = self.Q_table[self.states.index(s)]
        updates = self.Q_table_updates[self.states.index(s)]
        if self.log:
            print('Method ChangeMappings ### State: {} --- State Index: {}'.format(s ,self.states.index(s)))
            print('Method ChangeMappings ### Q_Vals:  ',q_vals)
            print('Method ChangeMappings ### Updates: ',updates)

        actionIndex = self.maxChoice(q_vals, updates)

        nextAction = ACTIONS[actionIndex]

        return nextAction

    def maxChoice(self,q_vals,updates):
        """ There is a EPSILON chance of choosing other than the best action
            so the algorithm can explore different decisions """
        r = np.random.random_sample()

        notExplored = any(i<1 for i in updates)

        if notExplored:
            indexes = [i for i,v in enumerate(updates) if v < 1]
            nextAction = random.choice(indexes)
            if self.log:
                print("Method maxChoive ### Next Exploration Action:  ",nextAction)
        elif r < self.epsilon:
            nextAction = random.randrange(self.qtdActions)
            if self.log:
                print("Method maxChoive ### Next Randomized Action:  ",nextAction)
        else:
            nextAction = np.argmax(q_vals)
            if self.log:
                print("Method maxChoive ### Next Maximized Action:  ",nextAction)
        return nextAction

    """ Getters and setters. Pretty much self explanatory """
    def SetState(self, s):
        self.curState = s

    def GetState(self):
        return self.curState

    def SetAction(self, a):
        self.lastAction = a

    def GetAction(self):
        return self.lastAction

    def SetBinSize(self, size, parameter):
        self.binSize[PARAMETERS.index(parameter)] = size

        ## Update the number of bins necessary given the max value ##
        ## for the parameter and its bin size                      ##
        maxValue = self.maxValue[PARAMETERS.index(parameter)]
        binSize = self.binSize[PARAMETERS.index(parameter)]
        self.bin_number[PARAMETERS.index(parameter)] = int(maxValue/binSize)

        self.qtdStates = np.prod(self.bin_number)

    def GetBinSize(self, parameter):
        return self.binSize[PARAMETERS.index(parameter)]

    def SetMaxValue(self, value, parameter):
        ## Set the max value for the corresponding parameter ##
        self.maxValue[PARAMETERS.index(parameter)] = value

        ## Update the number of bins necessary given the max value ##
        ## and the bin size                                        ##
        binSize = self.binSize[PARAMETERS.index(parameter)]
        self.bin_number[PARAMETERS.index(parameter)] = int(value/binSize)

        self.qtdStates = np.prod(self.bin_number)

    def GetWorkloadLog(self):
        return self.workloadLog

    def GetDelayLog(self):
        return self.delayLog

    def GetRewardLog(self):
        return self.rewardLog

    def GetUpdateLog(self):
        return self.updateLog

    def GetPowerLog(self):
        return self.powerLog

    def SetExploration(self, value):
        self.epsilon = value

    def SetLearningRate(self, value):
        self.alpha = value

    def SetDiscountRate(self, value):
        self.gamma = value

    def ClearLogs(self):
        self.workloadLog = []
        self.violationLog = []
        self.rewardLog = []
        self.updateLog = []
        self.powerLog = []

    def GenerateStates(self):
        """ Generate the states for the Q-Table according with the number of bins used for each parameter """
        self.states = [(w,cm) for w in range(1,self.bin_number[0]+1) for cm in range(1,self.bin_number[1]+1)]

    def Learn(self, p95th, power, workload, mapping):
        if self.log:
            print('Learn Function\n\n')

        # s_prime = (self.Discretize(workload,"Workload"), self.Discretize(delay,"Delay"), self.Discretize(mapping,"Core-Mapping"))
        s_prime = (self.Discretize(workload,"Workload"), self.Discretize(mapping,"Core-Mapping"))       
        a = self.GetAction()
        s = self.GetState()
        if s is not None:
            self.Q(s,a,s_prime,p95th,power)
            self.workloadLog.append(workload)
            self.violationLog.append(p95th)

        self.SetState(s_prime)


    def Decide(self,workload, cur_mapping):
        if self.log:
            print('Decide Function\n\n')
        s = (self.Discretize(workload,"Workload"), self.Discretize(cur_mapping,"Core-Mapping"))
        # s = self.Discretize(workload,"Workload")
        # self.SetAction(self.ChangeThresholds(s))
        self.SetAction(self.ChangeMappings(s))

        for new_mapping, intRepr in CORE_MAPPING.items():
            if intRepr == self.GetAction():
                if type(cur_mapping) == list: # If we are sending the results to the octopus, send as the list ([0,1])
                    return list(map(int,new_mapping[1:-1].split(",")))
                else: # If we are sending the results to the simulator, send as an int 0,1,2 or 3
                    return intRepr-1


    def Exit(self):
        maxQPS = str(self.maxValue[PARAMETERS.index("Workload")])
        binSize = str(self.binSize[PARAMETERS.index("Workload")])
        np.save(GLOBAL_PATH+'/octopusman/control/Q_table-'+self.application+'-'+maxQPS+'-'+binSize, self.Q_table)
        np.save(GLOBAL_PATH+'/octopusman/control/Q_table_updates-'+self.application+'-'+maxQPS+'-'+binSize, self.Q_table_updates)
        np.set_printoptions(suppress=True)

        RL_PolicyLog = open(os.path.join('/ramcache/logs','RL_Policy.log'),'w')

        for i in range(len(self.violationLog)):
            RL_PolicyLog.write(str(self.workloadLog[i])+' '+str(self.violationLog[i])+' '+str(self.rewardLog[i])+' '+str(self.updateLog[i])+'\n')
        RL_PolicyLog.close()

        print(self.Q_table)



