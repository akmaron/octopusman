#!/bin/bash

# Set the environment variables needed for PARSEC, POET, and Heartbeats.
# Run this script with ". ./setenv.sh". You must run with the dot before to
# set the environment variables for your current working instance of the shell
#
# Created by Philip Ni (5/21/2015)

pushd .
Color_Off='\e[0m' # Text Reset

bldgrn='\e[1;32m' # Green
bldylw='\e[1;33m' # Yellow
bldblu='\e[1;34m' # Blue
bldcyn='\e[1;36m' # Cyan

echo -e "\nSet the environement variables needed for PARSEC, POET, and Heartbeats."
#echo -e ${bldcyn}"Run this script with \". ./setenv.sh\". You must run with the dot before to set the environment variables for your current working instance of the shell.\n"${Color_Off}

#echo -e ${bldylw}"Setting LD_LIBRARY_PATH"${Color_Off}
#echo "Original LD_LIBRARY_PATH: "$LD_LIBRARY_PATH
cd apps/parsec-3.0
. ./env.sh
echo -e "\tRunning ~/parsec-3.0/env.sh: "$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:apps/heartbeats/lib:apps/poet/lib
echo -e ${bldblu}"\tLD_LIBRARY_PATH set: "$LD_LIBRARY_PATH ${Color_Off}

#echo -e ${bldylw}"\nSetting HEARTBEAT_ENABLED_DIR"${Color_Off}
#echo "Original HEARTBEAT_ENABLED_DIR: "$HEARTBEAT_ENABLED_DIR
#export HEARTBEAT_ENABLED_DIR=/tmp
export HEARTBEAT_ENABLED_DIR=/ramcache/logs
echo -e ${bldblu}"\tHEARTBEAT_ENABLED_DIR set: "$HEARTBEAT_ENABLED_DIR ${Color_Off}
cd

alias open_blackscholes_src="vim apps/parsec-3.0/pkgs/apps/blackscholes/src/blackscholes.c"
alias open_bodytrack_src="vim apps/parsec-3.0/pkgs/apps/bodytrack/src/TrackingBenchmark/main.cpp"
alias open_facesim_src="vim apps/parsec-3.0/pkgs/apps/facesim/src/Benchmarks/facesim/main.cpp"
alias open_ferret_src="vim apps/parsec-3.0/pkgs/apps/ferret/src/benchmark/ferret-pthreads.c"
alias open_x264_src="vim apps/parsec-3.0/pkgs/apps/x264/src/x264.c"

export HEART_BEAT_SRC_DIR=apps/heartbeats
export POET_SRC_DIR=apps/poet

export PARSECDIR=apps/parsec-3.0
export PATH=${PATH}:${PARSECDIR}/bin
export MANPATH=${MANPATH}:${PARSECDIR}/man
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${PARSECDIR}/pkgs/libs/hooks/inst/${PARSECPLAT}/lib
export X264_LOG=/ramcache/logs/heartbeat.log
export BUFFER_DEPTH=1

popd
